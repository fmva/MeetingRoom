module.exports = {
  id: 100,
  id_meeting: 2,
  title: 'Long discussion',
  date: '2019-06-12',
  from: '10:30',
  to: '11:30',
  persons: [
    {
      id: 1,
      img: 'Dart',
      fam: 'Wader',
      personImg: '/UserImg/1819650.jpg'
    },
    {
      id: 2,
      img: 'Mike',
      fam: 'Tyson',
      personImg: '/UserImg/1834399.jpg'
    },
    {
      id: 3,
      img: 'Luk',
      fam: 'Skulker',
      personImg: '/UserImg/1963621.jpg'
    },
    {
      id: 4,
      img: 'Lady',
      fam: 'Gaga',
      personImg: '/UserImg/2065639.jpg'
    },
    {
      id: 5,
      img: 'Nell',
      fam: 'Liner',
      personImg: '/UserImg/2085246.jpg'
    }
  ]
};
