module.exports = [
  {
    id: 1,
    title: 'Washing room',
    amount: 'up to 10 employees',
    floor: 7
  },
  {
    id: 2,
    title: 'Yellow Boat',
    amount: '3 - 6 employees',
    floor: 7
  },
  {
    id: 3,
    title: 'Main room',
    amount: 'up to 20 employees',
    floor: 7
  },
  {
    id: 4,
    title: 'Pink room',
    amount: '3 - 6 employees',
    floor: 7
  },
  {
    id: 5,
    title: 'Garden room',
    amount: 'up to 10 employees',
    floor: 8
  },
  {
    id: 6,
    title: 'Red Fred',
    amount: '3 - 6 employees',
    floor: 8
  },
  {
    id: 7,
    title: 'Orange room',
    amount: 'up to 10 employees',
    floor: 8
  },
  {
    id: 8,
    title: 'Light room',
    amount: '3 - 6 employees',
    floor: 8
  }
];
