module.exports = [
  {
    id: 1,
    floor: '7 floor',
    main: [
      {
        id: 1,
        filled: []
      },
      {
        id: 2,
        filled: []
      },
      {
        id: 3,
        filled: []
      },
      {
        id: 4,
        filled: []
      }
    ]
  },
  {
    id: 2,
    floor: '8 floor',
    main: [
      {
        id: 5,
        filled: []
      },
      {
        id: 6,
        filled: []
      },
      {
        id: 7,
        filled: []
      },
      {
        id: 8,
        filled: []
      }
    ]
  }
];
