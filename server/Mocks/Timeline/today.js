module.exports = [
  {
    id: 1,
    floor: '7 floor',
    main: [
      {
        id: 1,
        filled: [
          {
            from: '09:00',
            to: '09:05',
            id: 1
          },
          {
            from: '09:10',
            to: '09:15',
            id: 2
          },
          {
            from: '09:30',
            to: '11:30',
            id: 3
          },
          {
            from: '12:10',
            to: '18:58',
            id: 4
          }
        ]
      },
      {
        id: 2,
        filled: [
          {
            from: '08:00',
            to: '23:00',
            id: 5
          }
        ]
      },
      {
        id: 3,
        filled: [
          {
            from: '09:00',
            to: '09:15',
            id: 6
          },
          {
            from: '09:45',
            to: '10:00',
            id: 7
          },
          {
            from: '12:10',
            to: '15:30',
            id: 8
          }
        ]
      },
      {
        id: 4,
        filled: [
          {
            from: '09:00',
            to: '09:15',
            id: 9
          },
          {
            from: '09:45',
            to: '10:00',
            id: 10
          },
          {
            from: '12:10',
            to: '15:30',
            id: 11
          }
        ]
      }
    ]
  },
  {
    id: 2,
    floor: '8 floor',
    main: [
      {
        id: 5,
        filled: [
          {
            from: '09:00',
            to: '09:15',
            id: 12
          },
          {
            from: '09:45',
            to: '10:00',
            id: 13
          },
          {
            from: '12:10',
            to: '15:30',
            id: 14
          }
        ]
      },
      {
        id: 6,
        filled: [
          {
            from: '09:00',
            to: '09:15',
            id: 15
          },
          {
            from: '09:45',
            to: '10:00',
            id: 16
          },
          {
            from: '12:10',
            to: '15:30',
            id: 17
          }
        ]
      },
      {
        id: 7,
        filled: [
          {
            from: '09:00',
            to: '09:15',
            id: 18
          },
          {
            from: '09:45',
            to: '10:00',
            id: 19
          },
          {
            from: '12:10',
            to: '15:30',
            id: 20
          }
        ]
      },
      {
        id: 8,
        filled: [
          {
            from: '09:00',
            to: '09:15',
            id: 21
          },
          {
            from: '09:45',
            to: '10:00',
            id: 22
          },
          {
            from: '12:10',
            to: '15:30',
            id: 23
          }
        ]
      }
    ]
  }
];
