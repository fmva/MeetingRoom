module.exports = [
  {
    id: 1,
    floor: '7 floor',
    main: [
      {
        id: 1,
        filled: [
          {
            from: '09:00',
            to: '09:15',
            id: 25
          }
        ]
      },
      {
        id: 2,
        filled: [
          {
            from: '08:00',
            to: '10:00',
            id: 26
          },
          {
            from: '11:00',
            to: '12:30',
            id: 27
          },
          {
            from: '12:30',
            to: '16:00',
            id: 28
          },
          {
            from: '16:00',
            to: '17:00',
            id: 29
          }
        ]
      },
      {
        id: 3,
        filled: [
          {
            from: '18:00',
            to: '22:30',
            id: 30
          }
        ]
      },
      {
        id: 4,
        filled: [
          {
            from: '15:00',
            to: '18:00',
            id: 31
          }
        ]
      }
    ]
  },
  {
    id: 2,
    floor: '8 floor',
    main: [
      {
        id: 5,
        filled: []
      },
      {
        id: 6,
        filled: []
      },
      {
        id: 7,
        filled: []
      },
      {
        id: 8,
        filled: [
          {
            from: '12:00',
            to: '18:00',
            id: 32
          }
        ]
      }
    ]
  }
];
