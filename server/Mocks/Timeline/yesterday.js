module.exports = [
  {
    id: 1,
    floor: '7 floor',
    main: [
      {
        id: 1,
        filled: [
          {
            from: '11:00',
            to: '13:15',
            id: 33
          },
          {
            from: '14:00',
            to: '16:15',
            id: 34
          },
          {
            from: '16:20',
            to: '17:00',
            id: 35
          }
        ]
      },
      {
        id: 2,
        filled: [
          {
            from: '11:00',
            to: '15:00',
            id: 36
          },
          {
            from: '18:00',
            to: '23:00',
            id: 37
          }
        ]
      },
      {
        id: 3,
        filled: [
          {
            from: '10:00',
            to: '18:30',
            id: 38
          }
        ]
      },
      {
        id: 4,
        filled: [
          {
            from: '10:00',
            to: '11:00',
            id: 39
          },
          {
            from: '13:00',
            to: '14:30',
            id: 40
          },
          {
            from: '17:00',
            to: '19:30',
            id: 41
          }
        ]
      }
    ]
  },
  {
    id: 2,
    floor: '8 floor',
    main: [
      {
        id: 5,
        filled: [
          {
            from: '11:00',
            to: '13:00',
            id: 42
          }
        ]
      },
      {
        id: 6,
        filled: [
          {
            from: '18:00',
            to: '19:00',
            id: 43
          },
          {
            from: '19:00',
            to: '20:00',
            id: 44
          },
          {
            from: '20:00',
            to: '21:30',
            id: 45
          }
        ]
      },
      {
        id: 7,
        filled: [
          {
            from: '10:00',
            to: '13:00',
            id: 46
          }
        ]
      },
      {
        id: 8,
        filled: [
          {
            from: '20:00',
            to: '22:00',
            id: 46
          }
        ]
      }
    ]
  }
];
