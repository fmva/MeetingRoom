const defaultRest = require('./Mocks/Details/default');

/**
 details REST API
 @param {object} serverApp - serverApp
 @return {undefined}
 */
const detailsRestApi = function(serverApp) {
  serverApp.get('/details/:date/:id/', function(req, res) {
    //const timeFrom = req.query.from;
    //const timeTo = req.query.to;
    //const date = req.params.date;
    //const id = req.params.id;

    //console.log('timeFrom', timeFrom);
    //console.log('timeTo', timeTo);
    //console.log('date', date);
    //console.log('id', id);
    res.json(defaultRest);
  });
};

module.exports = {
  detailsRestApi: detailsRestApi
};
