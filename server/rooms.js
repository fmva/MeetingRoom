const defaultRest = require('./Mocks/Rooms/default');

/**
 rooms REST API
 @param {object} serverApp - serverApp
 @return {undefined}
 */
const roomsRestApi = function(serverApp) {
  serverApp.get('/rooms', function(req, res) {
    res.json(defaultRest);
  });
};

module.exports = {
  roomsRestApi: roomsRestApi
};
