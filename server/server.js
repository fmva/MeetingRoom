const express = require('express');
const Timeline = require('./timeline');
const Details = require('./details');
const Rooms = require('./rooms');

const SERVER_PORT = 5555;
const serverApp = express();

/**
 Allow CORS requests
 @param {object} req - req
 @param {object} res - res
 @param {object} next - next
 @returns {undefined}
 */
const handleCORS = function(req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  next();
};

serverApp.use(express.static(__dirname));
serverApp.use(handleCORS);

Timeline.timelineRestApi(serverApp);
Details.detailsRestApi(serverApp);
Rooms.roomsRestApi(serverApp);

serverApp.listen(SERVER_PORT, function() {
  console.log('Starting server at http://localhost:' + SERVER_PORT);
});
