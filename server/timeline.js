const TodayRest = require('./Mocks/Timeline/today');
const YesterdayRest = require('./Mocks/Timeline/yesterday');
const TomorrowRest = require('./Mocks/Timeline/tomorrow');
const DefaultRest = require('./Mocks/Timeline/default');

/**
 Timeline REST API
 @param {object} serverApp - serverApp
 @return {undefined}
 */
const timelineRestApi = function(serverApp) {
  serverApp.get('/timeline/:date/', function(req, res) {
    // setTimeout(()=>{
    const date = req.params.date;
    const year = parseInt(date.split('-')[0]);
    const month = parseInt(date.split('-')[1] - 1);
    const day = parseInt(date.split('-')[2]);

    const currentDate = new Date();
    const yesterdayDate = new Date();
    const tomorrowDate = new Date();
    yesterdayDate.setDate(yesterdayDate.getDate() - 1);
    tomorrowDate.setDate(tomorrowDate.getDate() + 1);

    if (
      currentDate.getFullYear() === year &&
      currentDate.getMonth() === month &&
      currentDate.getDate() === day
    ) {
      res.json(TodayRest);
    } else if (
      yesterdayDate.getFullYear() === year &&
      yesterdayDate.getMonth() === month &&
      yesterdayDate.getDate() === day
    ) {
      res.json(YesterdayRest);
    } else if (
      tomorrowDate.getFullYear() === year &&
      tomorrowDate.getMonth() === month &&
      tomorrowDate.getDate() === day
    ) {
      res.json(TomorrowRest);
    } else {
      res.json(DefaultRest);
    }
    // }, 4000);
  });
};

module.exports = {
  timelineRestApi: timelineRestApi
};
