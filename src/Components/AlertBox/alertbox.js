import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import MessageBox from '../MessageBox/messagebox';
import Button from '../Button/button';
import './alertbox.scss';

/**
 AlertBox container
 */
class AlertBox extends Component {
  /**
   render text content
   @param {string} headerText - text of header
   @param {object} bodyContent - text of body
   @return {Object} JSX object
   */
  renderTextContent(headerText, bodyContent = null) {
    return (
      <React.Fragment>
        <h1 className="header-modal-block">{headerText}</h1>
        {bodyContent}
      </React.Fragment>
    );
  }

  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { type, onCloseClick, headerText, bodyContent, tittleButton } = this.props;

    return (
      <MessageBox
        headerContent={
          type === 'error' ? <i className="icon-error" /> : <i className="icon-celebrate" />
        }
        bodyContent={this.renderTextContent(headerText, bodyContent)}
        btnContent={onCloseClick ? <Button text={tittleButton} onClick={onCloseClick} /> : null}
      />
    );
  }
}

AlertBox.defaultProps = {
  onCloseClick: null,
  type: 'success',
  headerText: '',
  bodyContent: null,
  tittleButton: ''
};

AlertBox.propTypes = {
  /** click close event*/
  onCloseClick: PropTypes.func,
  /** type of alert*/
  type: PropTypes.oneOf(['error', 'success']),
  /** body block content */
  bodyContent: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  /** header block content */
  headerText: PropTypes.string,
  /** title Button */
  tittleButton: PropTypes.string
};

export default AlertBox;
