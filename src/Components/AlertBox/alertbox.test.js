import AlertBox from './alertbox';
import React from 'react';
import { shallow } from 'enzyme';

describe('AlertBox', () => {
  it('error message', () => {
    const wrapperShallow = shallow(<AlertBox type={'error'} headerText={'headerText'} />);
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('success message', () => {
    const wrapperShallow = shallow(
      <AlertBox type={'success'} tittleButton={'OK'} headerText={''} />
    );
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('body content', () => {
    const wrapperShallow = shallow(
      <AlertBox type={'success'} tittleButton={'OK'} bodyContent={<div>content</div>} />
    );
    expect(wrapperShallow).toMatchSnapshot();
  });
});
