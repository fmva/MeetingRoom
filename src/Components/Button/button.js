import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import '../Styles/styles.scss';
import './button.scss';

/**
 Button container
 */
class Button extends Component {
  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { text, type, onClick } = this.props;

    return (
      <div>
        <button className={type === 'blue' ? 'btn' : 'btn-gray'} title={text} onClick={onClick}>
          {text}
        </button>
      </div>
    );
  }
}

Button.defaultProps = {
  text: '',
  type: 'blue',
  onClick: null
};

Button.propTypes = {
  /** button title*/
  text: PropTypes.string.isRequired,
  /** content for right side*/
  type: PropTypes.oneOf(['blue', 'gray']),
  /** click event*/
  onClick: PropTypes.func
};

export default Button;
