import Button from './button';
import React from 'react';
import { shallow } from 'enzyme';

describe('Button', () => {
  it('blue button', () => {
    const wrapperShallow = shallow(<Button text={'blue button'} />);
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('gray button', () => {
    const wrapperShallow = shallow(<Button text={'gray button'} type={'gray'} />);
    expect(wrapperShallow).toMatchSnapshot();
  });
});
