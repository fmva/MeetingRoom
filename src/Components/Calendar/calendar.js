import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import CircleButton from '../CircleButton/circlebutton';
import { getAbbreviateDays } from './calendar.resources';
import { getMonthsByNum } from '../Utils/date.resources';
import {
  addMonth,
  subtractMonth,
  getDaysFromMonth,
  isCurrentDate,
  setDayIntoDate
} from './calendar.model';
import '../Styles/styles.scss';
import './calendar.scss';

let wrapperRef;

/**
 Calendar container
 */
class Calendar extends Component {
  /**
   * @param {object} props - properties
   component's constructor
   */
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(props.date.getTime())
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
    wrapperRef = React.createRef();
  }

  /**
   componentDidMount life cycle method
   @returns {undefined}
   */
  async componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  /**
   componentWillUnmount life cycle method
   @return {undefined}
   */
  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  /**
   handle when user is clicked outside component
   @param {object} e - component's object
   @return {undefined}
   */
  handleClickOutside(e) {
    if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
      if (this.props.onCloseEvent()) {
        this.props.onCloseEvent();
      }
    }
  }

  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { onClickDay, coordinates, date } = this.props;
    return (
      <div
        ref={wrapperRef}
        className="calendar"
        style={{
          left: coordinates.left,
          top: coordinates.top
        }}
      >
        <div className="calendar-inner">
          <div className="calendar-months">
            <div className="date-block">
              <CircleButton
                type="arrow-left"
                classColor="calendar-color"
                onClick={() => {
                  this.setState({
                    date: new Date(subtractMonth(this.state.date))
                  });
                }}
              />
              <div className="date-title-block">
                <span className="day-calendar">{this.state.date.getFullYear()}</span>
                <span className="dot-before">
                  {getMonthsByNum(this.state.date.getMonth().toString())}
                </span>
              </div>
              <CircleButton
                type="arrow-right"
                classColor="calendar-color"
                onClick={() => {
                  this.setState({
                    date: new Date(addMonth(this.state.date))
                  });
                }}
              />
            </div>
          </div>
          <div className="calendar-dates-row">
            {getAbbreviateDays().map((item, index) => (
              <div key={index}>{item}</div>
            ))}
          </div>

          {getDaysFromMonth(this.state.date).map((item, index) => {
            return (
              <div key={`dates-row-${index}`} className="calendar-dates-row">
                {item.map((item2, index2) => {
                  if (item2 !== 0) {
                    return (
                      <div
                        key={`dates-day-${index2}`}
                        onClick={() => {
                          onClickDay && onClickDay(setDayIntoDate(item2, this.state.date));
                        }}
                        className={`calendar-day ${
                          isCurrentDate(item2, this.state.date, date) ? 'calendar-current-day' : ''
                        }`}
                      >
                        {item2}
                      </div>
                    );
                  } else {
                    return <div key={`dates-day-${index2}`} />;
                  }
                })}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

Calendar.defaultProps = {
  date: new Date(),
  onClickDay: null,
  onCloseEvent: null,
  coordinates: {
    left: 0,
    top: 0
  }
};

Calendar.propTypes = {
  /** date*/
  date: PropTypes.object,
  /** day click event */
  onClickDay: PropTypes.func,
  /** close event */
  onCloseEvent: PropTypes.func,
  /** coordinates */
  coordinates: PropTypes.object
};

export default Calendar;
