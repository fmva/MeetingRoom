/**
 * add one moth to current date
 * @param {object} date - current date
 * @return {object} - new date with added date
 */
export function addMonth(date) {
  const dt = new Date(date.getTime());
  dt.setMonth(dt.getMonth() + 1);
  return dt;
}

/**
 * subtract one moth from current date
 * @param {object} date - current date
 * @return {object} - new date with subtracted date
 */
export function subtractMonth(date) {
  const dt = new Date(date.getTime());
  dt.setMonth(dt.getMonth() - 1);
  return dt;
}

/**
 * get array with days from month for rendering
 * @param {object} date - current date
 * @return {array} - array with days
 */
export function getDaysFromMonth(date) {
  const dt = new Date(date.getFullYear(), date.getMonth(), 1);

  const firstDay = dt.getDay() === 0 ? 7 : dt.getDay();
  let days = [];
  let i,
    j = 0;

  days.push([]);
  for (i = 0; i < firstDay - 1; i++) {
    days[0].push(0);
  }

  while (dt.getMonth() === date.getMonth()) {
    days[j].push(dt.getDate());
    dt.setDate(dt.getDate() + 1);
    i++;
    if (i === 7) {
      i = 0;
      j++;
      days[j] = [];
    }
  }

  if (days[j].length < 7) {
    while (i !== 7) {
      days[j].push(0);
      i++;
    }
  }

  return days;
}

/**
 * check current date
 * @param {number} day - day of month
 * @param {object} date - date
 * @param {object} currentDate - current date
 * @return {boolean} - current or not current date
 */
export function isCurrentDate(day, date, currentDate = new Date()) {
  return (
    date.getFullYear() === currentDate.getFullYear() &&
    date.getMonth() === currentDate.getMonth() &&
    day === currentDate.getDate()
  );
}

/**
 * set day into date
 * @param {number} day - day of month
 * @param {object} date - date
 * @return {object} - new date
 */
export function setDayIntoDate(day, date) {
  return new Date(date.getFullYear(), date.getMonth(), day);
}
