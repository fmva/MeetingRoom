import {
  addMonth,
  subtractMonth,
  getDaysFromMonth,
  isCurrentDate,
  setDayIntoDate
} from './calendar.model';

describe('Calendar model tests', () => {
  it('addMonth function', () => {
    expect(addMonth(new Date(2019, 0, 1)).getMonth()).toBe(1);
    expect(addMonth(new Date(2019, 1, 1)).getMonth()).toBe(2);
  });

  it('subtractMonth function', () => {
    expect(subtractMonth(new Date(2019, 1, 1)).getMonth()).toBe(0);
    expect(subtractMonth(new Date(2019, 0, 1)).getMonth()).toBe(11);
  });

  it('getDaysFromMonth function', () => {
    const expected = [
      [0, 0, 0, 0, 1, 2, 3],
      [4, 5, 6, 7, 8, 9, 10],
      [11, 12, 13, 14, 15, 16, 17],
      [18, 19, 20, 21, 22, 23, 24],
      [25, 26, 27, 28, 0, 0, 0]
    ];
    expect(getDaysFromMonth(new Date(2019, 1, 1))).toEqual(expect.arrayContaining(expected));
  });

  it('isCurrentDate function', () => {
    expect(isCurrentDate(1, new Date(2019, 1, 1), new Date(2019, 1, 1))).toBeTruthy();
    expect(isCurrentDate(2, new Date(2019, 1, 1), new Date(2019, 1, 1))).toBeFalsy();
    expect(isCurrentDate(1, new Date(2019, 2, 1), new Date(2019, 1, 1))).toBeFalsy();
    expect(isCurrentDate(1, new Date(2018, 1, 1), new Date(2019, 1, 1))).toBeFalsy();
  });

  it('setDayIntoDate function', () => {
    expect(setDayIntoDate(1, new Date(2019, 2, 1)).getDate()).toBe(1);
    expect(setDayIntoDate(1, new Date(2019, 2, 1)).getFullYear()).toBe(2019);
    expect(setDayIntoDate(1, new Date(2019, 2, 1)).getMonth()).toBe(2);
    expect(setDayIntoDate(12, new Date(2019, 2, 1)).getDate()).toBe(12);
  });
});
