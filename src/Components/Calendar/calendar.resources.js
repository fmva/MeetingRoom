const days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

/**
 * abbreviate days
 * @return {array} abbreviate days
 */
export function getAbbreviateDays() {
  return days;
}
