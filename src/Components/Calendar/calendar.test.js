import Calendar from './calendar';
import React from 'react';
import { shallow } from 'enzyme';

describe('Calendar', () => {
  it('Calendar with data', () => {
    const wrapperShallow = shallow(<Calendar date={new Date(2018, 4, 5)} />);
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('Calendar with data and coordinates', () => {
    const wrapperShallow = shallow(
      <Calendar date={new Date(2018, 4, 5)} coordinates={{ top: 10, left: 20 }} />
    );
    expect(wrapperShallow).toMatchSnapshot();
  });
});
