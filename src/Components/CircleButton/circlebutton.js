import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import './circlebutton.scss';

/**
 CircleButton container
 */
class CircleButton extends Component {
  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { type, classColor, onClick } = this.props;

    return (
      <button className={`circle ${classColor}`} onClick={onClick}>
        <i className={`icon ${type}`} />
      </button>
    );
  }
}

CircleButton.defaultProps = {
  classColor: 'main-color',
  type: 'close',
  onClick: null
};

CircleButton.propTypes = {
  /** circle color style*/
  classColor: PropTypes.string,
  /** content for right side*/
  type: PropTypes.oneOf(['arrow-right', 'arrow-left', 'update', 'close']).isRequired,
  /** click event*/
  onClick: PropTypes.func
};

export default CircleButton;
