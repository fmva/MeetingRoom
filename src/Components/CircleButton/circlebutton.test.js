import CircleButton from './circlebutton';
import React from 'react';
import { shallow } from 'enzyme';

describe('CircleButton', () => {
  it('arrow left button', () => {
    const wrapperShallow = shallow(<CircleButton type={'arrow-left'} />);
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('arrow right button', () => {
    const wrapperShallow = shallow(<CircleButton type={'arrow-right'} />);
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('close button', () => {
    const wrapperShallow = shallow(<CircleButton type={'close'} />);
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('update button', () => {
    const wrapperShallow = shallow(<CircleButton type={'update'} />);
    expect(wrapperShallow).toMatchSnapshot();
  });
});
