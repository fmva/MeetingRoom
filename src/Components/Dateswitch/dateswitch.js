import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import { getTitleDay, getAbrDays } from './dateswitch.resources';
import { getMonthsByNum } from '../Utils/date.resources';
import { getDayText } from './dateswitch.model';
import CircleButton from '../CircleButton/circlebutton';
import Calendar from '../Calendar/calendar';
import './dateswitch.scss';
import '../Styles/styles.scss';

/**
 Header container
 */
class DateSwitch extends Component {
  /**
   * @param {object} props - properties
   component's constructor
   */
  constructor(props) {
    super(props);
    this.state = {
      isCalendarOpen: false
    };
  }

  /**
   close calendar
   @return {undefined} 
   */
  closeCalendar() {
    this.setState({
      isCalendarOpen: false
    });
  }

  /**
   open calendar
   @return {undefined}
   */
  openCalendar() {
    this.setState({
      isCalendarOpen: true
    });
  }

  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { onClickCalendarDay, onClickRightButton, onClickLeftButton, date } = this.props;
    const dayText = getDayText(date);

    return (
      <div className="date-switch-main">
        <div className="date-switch-block">
          <div>
            <CircleButton
              type="arrow-left"
              onClick={e => {
                onClickLeftButton && onClickLeftButton(e);
              }}
            />
          </div>
          <div
            className="date-switch-title"
            onClick={() => {
              this.openCalendar();
            }}
          >
            <span>{`${date.getDate()} ${
              dayText === 0
                ? getMonthsByNum(date.getMonth().toString())
                : getTitleDay(date.getMonth().toString())
            }`}</span>
            {dayText !== 0 ? (
              <span className="dot-before">{getAbrDays(dayText.toString())}</span>
            ) : null}
          </div>
          <div>
            <CircleButton
              type="arrow-right"
              onClick={e => {
                onClickRightButton && onClickRightButton(e);
              }}
            />
          </div>
        </div>
        {this.state.isCalendarOpen && (
          <Calendar
            date={date}
            isOpen={true}
            onClickDay={date => {
              onClickCalendarDay && onClickCalendarDay(date);
              this.closeCalendar();
            }}
            onCloseEvent={() => {
              this.closeCalendar();
            }}
          />
        )}
      </div>
    );
  }
}

DateSwitch.defaultProps = {
  onClickLeftButton: null,
  onClickRightButton: null,
  onClickCalendarDay: null,
  date: new Date()
};

DateSwitch.propTypes = {
  /** click event on left button*/
  onClickLeftButton: PropTypes.func,
  /** click event on right button*/
  onClickRightButton: PropTypes.func,
  /** click event on calendar day*/
  onClickCalendarDay: PropTypes.func,
  /** date (Date object)*/
  date: PropTypes.object
};

export default DateSwitch;
