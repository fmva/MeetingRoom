/**
 * get title of day (today,tomorrow, yesterday)
 * @param {Date} date - date
 * @param {Date} currentDate - current time
 * @return {number} - 0 others days, 1 - today, 2 - tomorrow, 3 - yesterday
 */
export function getDayText(date, currentDate = new Date()) {
  let dateB = new Date(date.getFullYear(), date.getMonth(), date.getDate());

  switch (dateB.getTime()) {
    case new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      currentDate.getDate()
    ).getTime():
      return 1;
    case new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      currentDate.getDate() + 1
    ).getTime():
      return 2;
    case new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      currentDate.getDate() - 1
    ).getTime():
      return 3;
    default:
      return 0;
  }
}
