import { getDayText } from './dateswitch.model';

describe('DateSwitch', () => {
  it('get title of day (today,tomorrow, yesterday)', () => {
    expect(getDayText(new Date(2018, 4, 12), new Date(2018, 4, 12))).toBe(1);
    expect(getDayText(new Date(2018, 4, 13), new Date(2018, 4, 12))).toBe(2);
    expect(getDayText(new Date(2018, 4, 11), new Date(2018, 4, 12))).toBe(3);
    expect(getDayText(new Date(2018, 4, 1), new Date(2018, 4, 12))).toBe(0);
    expect(getDayText(new Date(2018, 4, 14), new Date(2018, 4, 12))).toBe(0);
    expect(getDayText(new Date(2018, 4, 10), new Date(2018, 4, 12))).toBe(0);

    expect(getDayText(new Date(2019, 5, 1), new Date(2019, 4, 31))).toBe(2);
    expect(getDayText(new Date(2019, 4, 30), new Date(2019, 4, 31))).toBe(3);
  });
});
