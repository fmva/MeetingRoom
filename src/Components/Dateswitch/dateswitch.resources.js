const titleDay = {
  '1': 'Today',
  '2': 'Tomorrow',
  '3': 'Yesterday'
};

const titleAbrMonths = {
  '0': 'Jan',
  '1': 'Feb',
  '2': 'Mar',
  '3': 'Apr',
  '4': 'May',
  '5': 'June',
  '6': 'July',
  '7': 'Aug',
  '8': 'Sept',
  '9': 'Oct',
  '10': 'Nov',
  '11': 'Dec'
};

/**
 * get abbreviates of days
 * @param {string} num - number of month
 * @return {string} - abbreviates
 */
export function getAbrDays(num) {
  return titleDay[num];
}

/**
 * get text title of day
 * @param {string} num - number of title
 * @return {string} - title
 */
export function getTitleDay(num) {
  return titleAbrMonths[num];
}
