import DateSwitch from './dateswitch';
import React from 'react';
import { shallow } from 'enzyme';

describe('DateSwitch', () => {
  it('DateSwitch with setting date', () => {
    const wrapperShallow = shallow(<DateSwitch date={new Date(2018, 4, 5)} />);
    expect(wrapperShallow).toMatchSnapshot();
  });
});
