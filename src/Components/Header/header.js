import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import logo from './logo.jpg';
import './header.scss';

/**
 Header container
 */
class Header extends Component {
  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { isBottomBorder, rightContent } = this.props;

    return (
      <div className={isBottomBorder ? 'header' : null}>
        <div className="header-block">
          <div className="logo-block">
            <img src={logo} />
          </div>
          {rightContent ? <div className="right-header-block">{rightContent}</div> : null}
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  isBottomBorder: true,
  rightContent: null
};

Header.propTypes = {
  /** draw bottom border*/
  isBottomBorder: PropTypes.bool,
  /** content for right side*/
  rightContent: PropTypes.object
};

export default Header;
