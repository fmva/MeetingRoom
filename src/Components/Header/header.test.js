import Header from './header';
import React from 'react';
import { shallow } from 'enzyme';

describe('Header', () => {
  it('Show bottom line of Header', () => {
    const wrapperShallow = shallow(<Header isBottomBorder={true} />);
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('Hide bottom line of Header', () => {
    const wrapperShallow = shallow(<Header isBottomBorder={false} />);
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('Add content to the right side', () => {
    const wrapperShallow = shallow(<Header rightContent={<div className="text">test</div>} />);
    expect(wrapperShallow).toMatchSnapshot();
  });
});
