import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import './messagebox.scss';

/**
 MessageBox container
 */
class MessageBox extends Component {
  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { btnContent, bodyContent, headerContent } = this.props;

    return (
      <div className="modal-box">
        <div className="modal-box-inner">
          {headerContent ? <div className="header-modal-box">{headerContent}</div> : null}
          {bodyContent ? <div className="body-modal-box">{bodyContent}</div> : null}
          {btnContent ? <div className="btn-modal-block">{btnContent}</div> : null}
        </div>
      </div>
    );
  }
}

MessageBox.defaultProps = {
  btnContent: null,
  bodyContent: null,
  headerContent: null
};

MessageBox.propTypes = {
  /** button block content */
  btnContent: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  /** body block content */
  bodyContent: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  /** header block content */
  headerContent: PropTypes.oneOfType([PropTypes.object, PropTypes.string])
};

export default MessageBox;
