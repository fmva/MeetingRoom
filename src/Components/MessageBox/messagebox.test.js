import MessageBox from './messagebox';
import React from 'react';
import { shallow } from 'enzyme';

describe('MessageBox', () => {
  it('MessageBox with all props', () => {
    const wrapperShallow = shallow(
      <MessageBox btnContent="btnContent" bodyContent="bodyContent" headerContent="headerContent" />
    );
    expect(wrapperShallow).toMatchSnapshot();
  });

  it('MessageBox without all props', () => {
    const wrapperShallow = shallow(<MessageBox />);
    expect(wrapperShallow).toMatchSnapshot();
  });
});
