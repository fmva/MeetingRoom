export default {
  halfSizeTimerBlock: 25, //half size for current timer block
  paddingLeftHeaderMobile: 10, // padding for headers when scroll is shifted
  minTimeInterval: 5 // minimum interval of time between two time item (in minutes)
};
