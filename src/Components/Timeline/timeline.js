import React, { Component } from 'react';
import {
  getCurrentTimeState,
  getListTypes,
  fillItemObject,
  getListZoneTypes,
  timeListRestrictions,
  getMobileScrollCoordinates,
  getMobileScrollCoordinateByID,
  resetOpacityScrollCoordinate,
  shiftScrollCoordinate
} from './timeline.model';
import PropTypes from 'prop-types';
import Config from './timeline.config';
import './timeline.scss';
import Header from '../Header/header';

const halfSizeTimerBlock = Config.halfSizeTimerBlock;
const paddingLeftHeaderMobile = Config.paddingLeftHeaderMobile; //px

const oneMinute = 1000 * 60;
const timeOutShiftHeaders = 300; //msc

let timeZoneItemsObj = {};
let dataList = {};
let timestamp = 0;
let clickFilledZone = null;
let clickUnFilledZone = null;

let fromTime = 0;
let toTime = 23;
let countHours = 23;
let showCurrentTime = false;
let refLeftBlock;
let timeOutParamShiftHeaders = null;

let listHeaderContentMobileShift = null;
let listHeaderContent = null;
let listMainContentMobileShift = null;
let listMainContent = null;

/**
 Timeline component
 */
class Timeline extends Component {
  intervalTime = null;

  /**
   component's constructor
   */
  constructor() {
    super();
    refLeftBlock = React.createRef();
    this.state = {
      leftTimePosition: 0,
      currentTimeTitle: '00:00',
      pastTimes: [],
      scrollCoordinates: []
    };

    this.handleMainScroll = this.handleMainScroll.bind(this);
    this.hideScrollHeader = this.hideScrollHeader.bind(this);
  }

  /**
   componentDidMount life cycle method
   @returns {undefined}
   */
  componentDidMount() {
    console.log('componentDidMount');

    let newScrollCoordinates = getMobileScrollCoordinates(dataList);

    window.addEventListener('resize', this.hideScrollHeader);
    if (showCurrentTime) {
      let currentTimeState = getCurrentTimeState(fromTime, toTime);

      this.setState({
        leftTimePosition: currentTimeState.leftTimePosition,
        currentTimeTitle: currentTimeState.currentTimeTitle,
        pastTimes: currentTimeState.pastTimes,
        scrollCoordinates: newScrollCoordinates
      });
      this.interalTime = setInterval(() => {
        let currentTimeState = getCurrentTimeState(fromTime, toTime);

        this.setState({
          leftTimePosition: currentTimeState.leftTimePosition,
          currentTimeTitle: currentTimeState.currentTimeTitle,
          pastTimes: currentTimeState.pastTimes
        });
        if (currentTimeState.currentHour === toTime) {
          clearInterval(this.intervalTime);
        }
      }, oneMinute);
    }
  }

  /**
   componentWillUnmount life cycle method
   @returns {undefined}
   */
  componentWillUnmount() {
    clearInterval(this.interalTime);
    window.removeEventListener('resize', this.hideScrollHeader);
  }

  /**
   hide items headers
   @returns {undefined}
   */
  hideScrollHeader() {
    this.setState({
      scrollCoordinates: resetOpacityScrollCoordinate(this.state.scrollCoordinates)
    });
  }

  /**
   scroll handle
   @param {Object} e -  scroll element object
   @returns {undefined}
   */
  handleMainScroll(e) {
    let offsetWidth = refLeftBlock.current.offsetWidth;
    let scrollLeft = e.target.scrollLeft;

    if (scrollLeft === 0) {
      return;
    }

    clearTimeout(timeOutParamShiftHeaders);
    timeOutParamShiftHeaders = setTimeout(() => {
      this.setState({
        scrollCoordinates: shiftScrollCoordinate(
          this.state.scrollCoordinates,
          scrollLeft,
          offsetWidth,
          paddingLeftHeaderMobile
        )
      });
    }, timeOutShiftHeaders);
  }

  /**
   render title time
   @param {number} time -  current time
   @param {number} from - lower boundary time
   @param {array} arrPastTime - time which lower current boundary
   @returns {Object} JSX object
   */
  renderTitleTime(time, from, arrPastTime = []) {
    return (
      <span
        className={'clock-time-item ' + (arrPastTime.indexOf(time) !== -1 ? 'past-time-item' : '')}
      >
        {time === from ? time + ':00' : time}
      </span>
    );
  }

  /**
   render time header list
   @param {number} from -  from time label
   @param {number} to - to time label
   @param {array} arrPastTime - this array of hours is lower current hour
   @returns {Object} JSX object
   */
  renderHeaderTimeZone(from, to, arrPastTime = []) {
    let timeZoneList = [];
    let stepTime = from;

    while (stepTime !== to) {
      timeZoneList.push(
        <div
          key={stepTime}
          className="time-zone-clock"
          style={{ width: 'calc(100%/' + countHours + ' + 1px)' }}
        >
          {this.renderTitleTime(stepTime, from, arrPastTime)}
        </div>
      );
      stepTime++;
    }

    return <React.Fragment>{timeZoneList}</React.Fragment>;
  }

  /**
   render time zone
   @param {number} from -  from time label
   @param {number} to - to time label
   @returns {Object} JSX object
   */
  renderBodyTimeZone(from, to) {
    let timeZoneList = [];
    let stepTime = from;
    while (stepTime !== to) {
      timeZoneList.push(
        <div
          key={stepTime}
          className="time-zone-border"
          style={{ width: 'calc(100%/' + countHours + ' + 1px)' }}
        />
      );
      stepTime++;
    }

    return <React.Fragment>{timeZoneList}</React.Fragment>;
  }

  /**
   render time line with current hours and minutes
   @param {number} left -  left coordinate of line
   @param {number} time - time
   @returns {Object} JSX object
   */
  renderTimeLine(left, time) {
    if (!left) {
      return null;
    }
    return (
      <div
        className="current-time-block"
        style={{ left: 'calc(' + left + '% - ' + halfSizeTimerBlock + 'px)' }}
      >
        <span>{time}</span>
      </div>
    );
  }

  /**
   render header list data
   @param {string} id -  key id
   @param {string[]|string} content -  content (JSX)
   @param {string[]|string} contentMobileShift -  content when user shift scroll (JSX)
   @returns {Object} JSX object
   */
  renderListHeader(id, content, contentMobileShift) {
    let scrollObj = this.state.scrollCoordinates
      ? getMobileScrollCoordinateByID(this.state.scrollCoordinates, id)
      : {};
    let meetingTitleBlockStyle = {
      left: scrollObj ? scrollObj.left : 0,
      opacity: scrollObj ? scrollObj.opacity : 0
    };

    return (
      <li key={`${getListTypes().header}${id}`}>
        <div className="meeting-rooms-header">
          <div className="block-left-floor">
            <div className="meeting-title-shift" style={meetingTitleBlockStyle}>
              {this.renderInputListHeaderMobileShift(contentMobileShift)}
            </div>
            {this.renderInputListHeader(content)}
          </div>
        </div>
      </li>
    );
  }

  /**
   render header list data
   @param {string} id -  key id
   @param {string[]|string} content -  content (JSX)
   @param {string[]|string} contentMobileShift -  content when user shift scroll (JSX)
   @returns {Object} JSX object
   */
  renderListMain(id, content, contentMobileShift) {
    let isFilled = timeZoneItemsObj[id] ? timeZoneItemsObj[id].filled : false;
    let scrollObj = this.state.scrollCoordinates
      ? getMobileScrollCoordinateByID(this.state.scrollCoordinates, id)
      : {};
    let meetingTitleBlockStyle = {
      left: scrollObj ? scrollObj.left : 0,
      opacity: scrollObj ? scrollObj.opacity : 0
    };

    return (
      <li key={`${getListTypes().main}${id}`}>
        <div className="block-left ">
          <div className={'meeting-title-block ' + (isFilled ? 'filled' : '')}>
            <div className="meeting-title-shift meeting-shift" style={meetingTitleBlockStyle}>
              {this.renderInputListMainMobileShift(contentMobileShift)}
            </div>
            {this.renderInputListMain(content)}
          </div>
        </div>
        <div className="time-body-columns-item">
          <div className="first-time-zone time-zone-border time-zone-close" />
          {this.renderItemTimeLine(id)}
          <div className="last-time-zone time-zone-close" />
        </div>
      </li>
    );
  }

  /**
   render item time line
   @param {string} id -  id
   @returns {Object} JSX object
   */
  renderItemTimeLine(id) {
    const fillInnerArr = timeZoneItemsObj[id] ? timeZoneItemsObj[id].inner : [];
    const fillOuterArr = timeZoneItemsObj[id] ? timeZoneItemsObj[id].outer : [];
    let itemsArr = [];
    let stepTime = fromTime;
    while (stepTime !== toTime) {
      itemsArr.push(
        <div
          key={stepTime}
          className="time-zone-border time-zone-item"
          style={{ width: 'calc(100%/' + countHours + ' + 1px)' }}
        >
          {this.renderInnerFilledTimeLine(id, fillInnerArr, stepTime)}
        </div>
      );
      stepTime++;
    }

    return (
      <div className="middle-time-zone-inner">
        {itemsArr} {this.renderOuterFilledTimeLine(id, fillOuterArr)}
      </div>
    );
  }

  /**
   render inner filled/unfilled time line
   @param {string} id -  id item
   @param {array} fillArr -  array with render properties
   @param {number} hour -  current hour
   @returns {Object} JSX object
   */
  renderInnerFilledTimeLine(id, fillArr, hour) {
    if (!fillArr[hour] || fillArr[hour].length === 0) {
      return null;
    }

    let arrFilled = [];

    fillArr[hour].forEach(item => {
      if (item.type === getListZoneTypes().unfilled) {
        arrFilled.push(
          <div
            onClick={() => (clickUnFilledZone ? clickUnFilledZone(id, item.from, item.to) : null)}
            key={`unfilled_${item.time}`}
            className="time-zone-unfilled-inner"
            style={{ width: item.width + '%', left: item.left + '%' }}
          />
        );
      } else if (item.type === getListZoneTypes().filled) {
        arrFilled.push(
          <div
            key={`filled_${item.time}`}
            className="time-zone-filled-inner"
            style={{ width: item.width + '%', left: item.left + '%' }}
          />
        );
      }
    });

    return <React.Fragment>{arrFilled}</React.Fragment>;
  }

  /**
   render outer filled time line
   @param {string} id -  id
   @param {array} fillArr -  array with render properties
   @returns {Object} JSX object
   */
  renderOuterFilledTimeLine(id, fillArr) {
    if (!fillArr || fillArr.length === 0) {
      return null;
    }

    let arrFilled = [];

    fillArr.forEach(item => {
      arrFilled.push(
        <div
          onClick={e => (clickFilledZone ? clickFilledZone(e, id, item.from, item.to) : null)}
          key={item.time}
          className="time-zone-filled-outer"
          style={{ width: item.width + '%', left: item.left + '%' }}
        />
      );
    });

    return <React.Fragment>{arrFilled}</React.Fragment>;
  }

  /**
   render list data
   @param {array} dataList -  list data
   @returns {Object} JSX object
   */
  renderList(dataList) {
    let arrList = [];

    dataList.forEach(item => {
      //if (item.type === getListTypes().header) {
      arrList.push(this.renderListHeader(item.id, item.content, item.contentMobileShift));
      item.main.forEach(itemMain => {
        arrList.push(
          this.renderListMain(itemMain.id, itemMain.content, itemMain.contentMobileShift)
        );
      });
    });

    return <ul className="meeting-rooms-list">{arrList}</ul>;
  }

  /**
   render header list content when user shift scroll in mobile version
   @param {string[]|string} titles - titles for shift
   @returns {Object} JSX object
   */
  renderInputListHeaderMobileShift(titles) {
    if (!listHeaderContentMobileShift) {
      return (
        <span className="floor-number">
          {typeof titles === 'string' ? titles : titles.join(' ')}
        </span>
      );
    } else {
      return listHeaderContentMobileShift(titles);
    }
  }

  /**
   render header list content 
   @param {string[]|string} titles - titles for shift
   @returns {Object} JSX object
   */
  renderInputListHeader(titles) {
    if (!listHeaderContent) {
      return (
        <div className="floor-block">
          <span className="floor-number">
            {typeof titles === 'string' ? titles : titles.join(' ')}
          </span>
        </div>
      );
    } else {
      return listHeaderContent(titles);
    }
  }

  /**
   render header list content when user shift scroll in mobile version
   @param {string[]|string} titles - titles for shift
   @returns {Object} JSX object
   */
  renderInputListMainMobileShift(titles) {
    if (!listMainContentMobileShift) {
      return (
        <span className="title-meeting-text">
          {typeof titles === 'string' ? titles : titles.join(' ')}
        </span>
      );
    } else {
      return listMainContentMobileShift(titles);
    }
  }

  /**
   render main list content
   @param {string[]|string} titles - titles for shift
   @returns {Object} JSX object
   */
  renderInputListMain(titles) {
    if (!listMainContent) {
      return (
        <div className="title-meeting-block">
          <div className="title-meeting">
            <span className="title-meeting-text">
              {typeof titles === 'string' ? titles : titles.join(' ')}
            </span>
          </div>
        </div>
      );
    } else {
      return listMainContent(titles);
    }
  }

  /**
   set time zone object
   * @param {number} innerTimestamp - timestamp
   * @return {undefined}
   */
  setTimeZoneObject(innerTimestamp) {
    if (innerTimestamp !== timestamp) {
      timestamp = innerTimestamp;
    } else {
      return;
    }

    console.time('filling zone time');
    dataList.forEach(item => {
      item.main.forEach(itemMain => {
        //dataListMain.push(itemMain);
        timeZoneItemsObj[itemMain.id] = fillItemObject(itemMain.filled, fromTime, toTime);
      });
    });
    console.log('timeZoneItemsObj', timeZoneItemsObj);
    console.timeEnd('filling zone time');
  }

  /**
    render component
    @returns {Object} JSX object
   */
  render() {
    console.log('timeline render');
    const { headerContent } = this.props;
    ({
      showCurrentTime,
      listHeaderContentMobileShift,
      listHeaderContent,
      listMainContentMobileShift,
      listMainContent
    } = this.props);
    dataList =
      this.props.dataList && this.props.dataList.data && this.props.dataList.data.length > 0
        ? this.props.dataList.data
        : [];
    let currentTimestamp =
      this.props.dataList && this.props.dataList.timestamp ? this.props.dataList.timestamp : 0;

    clickFilledZone = this.props.onClickFilledZone;
    clickUnFilledZone = this.props.onClickUnFilledZone;

    fromTime = this.props.timeList.from;
    toTime = this.props.timeList.to;
    timeListRestrictions(fromTime, toTime);

    this.setTimeZoneObject(currentTimestamp);
    countHours = toTime - fromTime;

    return (
      <div className="main-timeline" onScroll={this.handleMainScroll}>
        <div className="main-inner">
          <div className="time-header">
            <div className="is-desktop block-left-date">{headerContent}</div>
            <div className="time-body-main-clocks">
              <div className="block-left is-devices" />
              <div className="first-time-zone" />
              <div className="middle-time-zone-inner">
                {this.renderHeaderTimeZone(fromTime, toTime, this.state.pastTimes)}
                {this.renderTimeLine(this.state.leftTimePosition, this.state.currentTimeTitle)}
              </div>
              <div className="last-time-zone">
                <div className="time-zone-clock">
                  {this.renderTitleTime(toTime, fromTime, this.state.pastTimes)}
                </div>
              </div>
            </div>
          </div>

          <div className="time-body">
            <div className="meeting-list">
              <div className="meeting-list-inner">{this.renderList(dataList)}</div>
            </div>
            <div ref={refLeftBlock} className="block-left-design" />
            <div className="time-body-main time-zone-bg-main">
              <div className="time-body-columns">
                <div className="first-time-zone time-zone-border" />
                <div className="middle-time-zone">{this.renderBodyTimeZone(fromTime, toTime)}</div>
                <div className="last-time-zone" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  headerContent: null,
  showCurrentTime: false,
  timeList: {
    from: fromTime,
    to: toTime
  },
  onClickFilledZone: () => {},
  onClickUnFilledZone: () => {},
  dataList: {},
  listHeaderContentMobileShift: null,
  listHeaderContent: null,
  listMainContentMobileShift: null,
  listMainContent: null
};

Timeline.propTypes = {
  /** properties for time*/
  timeList: PropTypes.PropTypes.shape({
    /** from time*/
    from: PropTypes.number.isRequired,
    /** to time*/
    to: PropTypes.number.isRequired
  }).isRequired,
  /** show current time line*/
  showCurrentTime: PropTypes.bool,
  /** data list*/
  dataList: PropTypes.object,
  /** left header column content*/
  headerContent: PropTypes.object,
  /** Event click for filled zone*/
  onClickFilledZone: PropTypes.func,
  /** Event click for unfilled zone*/
  onClickUnFilledZone: PropTypes.func,
  /** function for header list content while user is scrolled*/
  listHeaderContentMobileShift: PropTypes.func,
  /** function for header list content */
  listHeaderContent: PropTypes.func,
  /** function for content list content while user is scrolled*/
  listMainContentMobileShift: PropTypes.func,
  /** function for header list main */
  listMainContent: PropTypes.func
};

export default Timeline;
