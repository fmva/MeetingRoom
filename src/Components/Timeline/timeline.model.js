const HOUR_TO_MINUTES = 60;
const HUNDRED_PERCENTAGE = 100;

const LIST_TYPES = {
  header: 'header',
  main: 'main'
};

const FILL_ZONE_TYPES = {
  unfilled: 'unfilled',
  filled: 'filled'
};
/*
const THROW_FILL_ERROR = {
  lengthArrayLowerLastIndexTime: {
    error: 1,
    message: 'Length meeting zone array is higher than toIndexTime parameter'
  },
  firstIndexTimeLowerZero: {
    error: 2,
    message: 'fromIndexTime is lower 0'
  },
  lastIndexTimeLowerFirstIndexTime: {
    error: 3,
    message: 'fromIndexTime is higher than toIndexTime'
  },

   // throw error if zone busy
  //  @param {string} time -  busy time
  //  @return {object} - error object

  zoneIsBusy(time) {
    return {
      error: 4,
      message: `Zone with time ${time} is busy!`
    };
  }
};
*/
const THROW_TIME_HEADER = {
  timeLessZero: {
    error: 0,
    message: 'Time is less 0!'
  },
  timeHigher23: {
    error: 1,
    message: 'Time is higher 23!'
  },
  higherHourLessLowerHour: {
    error: 1,
    message: 'Lower bounder time is higher than higher bounder!'
  }
};

/**
 * get diapason format time like HH:MM-HH:MM
 * @param {string} hourFirst -  first time
 * @param {string} hourSecond -  second time
 * @return {string} - format time HH:MM-HH:MM
 */
function timeDiapason(hourFirst, hourSecond) {
  return `${hourFirst}-${hourSecond}`;
}

/**
 * get diapason format time like HH:MM-HH:MM
 * @param {number} hourFrom -  first hour
 * @param {number} minutesFrom -  first minutes
 * @param {number} hourTo -  second hour
 * @param {number} minutesTo -  second minutes
 * @return {string} - format time HH:MM-HH:MM
 */
function timeDiapasonByMinutes(hourFrom, minutesFrom, hourTo, minutesTo) {
  return timeDiapason(formatTime(hourFrom, minutesFrom), formatTime(hourTo, minutesTo));
}

/**
 * parse diapazon like 'HH:MM'
 * @param {string} item -  time like 'HH:MM'
 * @return {array} - array with hour and minutes
 */
function parseTimeDiapazon(item) {
  return [parseInt(item.split(':')[0]), parseInt(item.split(':')[1])];
}

/**
 * format time to HH:MM
 * @param {number} hour -  hour
 * @param {number} minutes -  minutes
 * @return {string} - format time HH:MM
 */
function formatTime(hour, minutes) {
  return (hour < 10 ? '0' + hour : hour) + ':' + (minutes < 10 ? '0' + minutes : minutes);
}

/**
 * get left position for current time
 * @param {number} hour - current hour
 * @param {number} minutes - current minutes
 * @param {number} from - lower boundary hour
 * @param {number} to - higher boundary hour
 * @return {number} - left coordinate for time
 */
export function getLeftTimePosition(hour, minutes, from, to) {
  if (hour < from) {
    throw {
      message: 'Error boundary. Hour is less lower boundary',
      code: 1
    };
  } else if (hour >= to) {
    throw {
      message: 'Error boundary. Hour is more higher boundary',
      code: 2
    };
  }
  let diff = to - from;
  let diffHour = (hour - from) * 60;
  let currentMin = diffHour + minutes;
  return ((currentMin / 60) * 100) / diff;
}

/**
 * get current time state object
 * @param {number} fromTime - lower boundary hour
 * @param {number} toTime - higher boundary hour
 * @return {object} - state object
 */
export function getCurrentTimeState(fromTime, toTime) {
  let leftTimePosition;
  let pastTimes = [];
  let date = new Date();

  try {
    leftTimePosition = getLeftTimePosition(date.getHours(), date.getMinutes(), fromTime, toTime);
  } catch (e) {
    leftTimePosition = null;
  }

  for (let i = fromTime; i <= date.getHours(); i++) {
    pastTimes.push(i);
  }

  return {
    leftTimePosition: leftTimePosition,
    currentTimeTitle: formatTime(date.getHours(), date.getMinutes()),
    currentHour: date.getHours(),
    pastTimes: pastTimes
  };
}

/**
 * return list types
 * @return {object} - list types
 */
export function getListTypes() {
  return LIST_TYPES;
}

/**
 * return list zone types
 * @return {object} - list types
 */
export function getListZoneTypes() {
  return FILL_ZONE_TYPES;
}

/**
 * throw error if header time isn't correct
 * @param {number} fromTime - lower boundary hour
 * @param {number} toTime - higher boundary hour
 * @return {undefined}
 */
export function timeListRestrictions(fromTime, toTime) {
  if (fromTime < 0) {
    throw THROW_TIME_HEADER.timeLessZero;
  }
  if (toTime > 23) {
    throw THROW_TIME_HEADER.timeHigher23;
  }
  if (fromTime > toTime) {
    throw THROW_TIME_HEADER.higherHourLessLowerHour;
  }
}

/**
 * get mobile scroll coordinates
 * @param {array} dataList - array with id items
 * @return {array} - array with initial scroll coordinates
 */
export function getMobileScrollCoordinates(dataList) {
  let arr = [];
  dataList.forEach(item => {
    arr.push({
      id: item.id,
      left: 0,
      opacity: 0
    });
    item.main.forEach(itemMain => {
      arr.push({
        id: itemMain.id,
        left: 0,
        opacity: 0
      });
    });
  });

  return arr;
}

/**
 * get object scroll coordinates by id
 * @param {array} scrollCoordinates - array with initial scroll coordinates
 * @param {string} id - id item
 * @return {object} - scroll object coordinates
 */
export function getMobileScrollCoordinateByID(scrollCoordinates, id) {
  return scrollCoordinates.find(item => item.id === id);
}

/**
 * reset all opacity to zero
 * @param {array} scrollCoordinates - array with initial scroll coordinates
 * @return {array} - new scroll object coordinates where opacity is zero
 */
export function resetOpacityScrollCoordinate(scrollCoordinates) {
  return scrollCoordinates.map(item => {
    return Object.assign({}, item, { opacity: 0 });
  });
}

/**
 * shift scroll coordinates of block to left
 * @param {array} scrollCoordinates - array with initial scroll coordinates
 * @param {number} scrollLeft - left coordinates when scroll is changed
 * @param {number} widthLeftColumn - width left column
 * @param {number} paddingLeft - addition shift for column
 * @return {array} - new scroll object coordinates where opacity is zero
 */
export function shiftScrollCoordinate(
  scrollCoordinates,
  scrollLeft,
  widthLeftColumn,
  paddingLeft = 0
) {
  if (scrollLeft > widthLeftColumn) {
    return scrollCoordinates.map(item => {
      return Object.assign({}, item, { opacity: 1, left: scrollLeft + paddingLeft + 'px' });
    });
  } else {
    return scrollCoordinates.map(item => {
      return Object.assign({}, item, { opacity: 0, left: 0 });
    });
  }
}

/**
 * divide diapason which bigger one cells
 * @param {object} value - the value of diapason time
 * @return {array} - new diapason array
 */
function divideValuesByHours(value) {
  const arr = [];
  const [hourFrom, minutesFrom] = parseTimeDiapazon(value.from);
  const [hourTo, minutesTo] = parseTimeDiapazon(value.to);
  const fullMinutesTo = minutesTo === 0 ? HOUR_TO_MINUTES : minutesTo;

  if (hourFrom === hourTo || (hourFrom === hourTo - 1 && fullMinutesTo === HOUR_TO_MINUTES)) {
    arr.push([value.from, value.to]);
  } else {
    for (let i = hourFrom; i <= hourTo; i += 1) {
      if (i === hourFrom) {
        arr.push([formatTime(i, minutesFrom), formatTime(i + 1, 0)]);
      } else if (i !== hourTo) {
        arr.push([formatTime(i, 0), formatTime(i + 1, 0)]);
      } else if (i === hourTo && minutesTo !== 0) {
        arr.push([formatTime(i, 0), formatTime(hourTo, minutesTo)]);
      }
    }
  }

  return arr;
}

/**
 * inner object with filled coordinates
 * @param {array} dividedItem - array with divided values by cells
 * @param {object} innerOut - array with initial coordinate cells
 * @return {object} - object with coordinate of filled cells
 */
function getInnerCoordinates(dividedItem, innerOut) {
  const inner = innerOut;
  for (let i = 0; i < dividedItem.length; i++) {
    const newItem = dividedItem[i];

    const [hourFrom, minutesFrom] = parseTimeDiapazon(newItem[0]);
    const [hourTo, minutesTo] = parseTimeDiapazon(newItem[1]);
    const fullMinutesTo = minutesTo === 0 ? HOUR_TO_MINUTES : minutesTo;

    const lastIndex = inner[hourFrom][inner[hourFrom].length - 1];
    const [lastIndexHourFrom, lastIndexMinutesFrom] = parseTimeDiapazon(lastIndex.from);
    const [lastIndexHourTo, lastIndexMinutesTo] = parseTimeDiapazon(lastIndex.to);

    if (hourFrom === lastIndexHourFrom && minutesFrom === lastIndexMinutesFrom) {
      inner[hourFrom].pop();

      inner[hourFrom].push({
        time: timeDiapasonByMinutes(hourFrom, minutesFrom, hourTo, minutesTo),
        from: formatTime(hourFrom, minutesFrom),
        to: formatTime(hourTo, minutesTo),
        type: FILL_ZONE_TYPES.filled,
        left: (HUNDRED_PERCENTAGE * minutesFrom) / HOUR_TO_MINUTES,
        width: ((fullMinutesTo - minutesFrom) * HUNDRED_PERCENTAGE) / HOUR_TO_MINUTES,
        id: `${hourFrom}${minutesFrom}${hourTo}${minutesTo}`
      });

      if (hourTo + 1 !== lastIndexHourFrom && fullMinutesTo !== HOUR_TO_MINUTES) {
        lastIndex.time = timeDiapasonByMinutes(
          hourTo,
          minutesTo,
          lastIndexHourTo,
          lastIndexMinutesTo
        );
        lastIndex.from = formatTime(hourTo, minutesTo);
        lastIndex.width = ((HOUR_TO_MINUTES - minutesTo) * HUNDRED_PERCENTAGE) / HOUR_TO_MINUTES;
        lastIndex.left = (HUNDRED_PERCENTAGE * minutesTo) / HOUR_TO_MINUTES;
        lastIndex.id = lastIndex.time.replace(':', '').replace('-', '');
        inner[hourFrom].push(lastIndex);
      }
    } else if (hourFrom === lastIndexHourFrom && minutesFrom > lastIndexMinutesFrom) {
      lastIndex.time = timeDiapasonByMinutes(
        lastIndexHourFrom,
        lastIndexMinutesFrom,
        hourFrom,
        minutesFrom
      );
      lastIndex.to = formatTime(hourFrom, minutesFrom);
      lastIndex.width =
        ((minutesFrom - lastIndexMinutesFrom) * HUNDRED_PERCENTAGE) / HOUR_TO_MINUTES;
      lastIndex.id = lastIndex.time.replace(':', '').replace('-', '');

      inner[hourFrom].push({
        time: timeDiapasonByMinutes(hourFrom, minutesFrom, hourTo, minutesTo),
        from: formatTime(hourFrom, minutesFrom),
        to: formatTime(hourTo, minutesTo),
        type: FILL_ZONE_TYPES.filled,
        left: (HUNDRED_PERCENTAGE * minutesFrom) / HOUR_TO_MINUTES,
        width: ((fullMinutesTo - minutesFrom) * HUNDRED_PERCENTAGE) / HOUR_TO_MINUTES,
        id: `${hourFrom}${minutesFrom}${hourTo}${minutesTo}`
      });

      if (hourTo + 1 !== lastIndexHourFrom && fullMinutesTo !== HOUR_TO_MINUTES) {
        inner[hourFrom].push({
          time: timeDiapasonByMinutes(hourTo, minutesTo, hourTo + 1, 0),
          from: formatTime(hourTo, minutesTo),
          to: formatTime(hourTo + 1, 0),
          type: FILL_ZONE_TYPES.unfilled,
          left: (HUNDRED_PERCENTAGE * minutesTo) / HOUR_TO_MINUTES,
          width: ((HOUR_TO_MINUTES - minutesTo) * HUNDRED_PERCENTAGE) / HOUR_TO_MINUTES,
          id: `${hourTo}${minutesTo}${hourTo + 1}00`
        });
      }
    }
  }

  return inner;
}

/**
 * outer object with filled coordinates
 * @param {object} times - array with diapason time
 * @param {number} fromTime - lower boundary hour
 * @param {number} toTime - higher boundary hour
 * @return {object} - object with coordinate of filled cells
 */
function getOuterCoordinates(times, fromTime, toTime) {
  const id = times.id;
  const [hourFrom, minutesFrom] = parseTimeDiapazon(times.from);
  const [hourTo, minutesTo] = parseTimeDiapazon(times.to);
  const maxDiapazonMinutes = (toTime - fromTime) * HOUR_TO_MINUTES;

  return {
    time: timeDiapasonByMinutes(hourFrom, minutesFrom, hourTo, minutesTo),
    id: id,
    from: formatTime(hourFrom, minutesFrom),
    to: formatTime(hourTo, minutesTo),
    left:
      (HUNDRED_PERCENTAGE * ((hourFrom - fromTime) * HOUR_TO_MINUTES + minutesFrom)) /
      maxDiapazonMinutes,
    width:
      (HUNDRED_PERCENTAGE *
        ((hourTo - fromTime) * HOUR_TO_MINUTES +
          minutesTo -
          ((hourFrom - fromTime) * HOUR_TO_MINUTES + minutesFrom))) /
      maxDiapazonMinutes
  };
}

/**
 * return true if all cells was filled
 * @param {object} inner - object with filled or unfilled values
 * @return {boolean} - fill status
 */
function isFilled(inner) {
  let filled;

  for (let i in inner) {
    filled = inner[i].some(item => {
      return item.type === FILL_ZONE_TYPES.unfilled;
    });

    if (filled) {
      return false;
    }
  }

  return true;
}

/**
 * filling item object
 * @param {array} itemsTimeArray - array with diapason time
 * @param {number} fromTime - lower boundary hour
 * @param {number} toTime - higher boundary hour
 * @return {object} - object with filling properties
 */
export function fillItemObject(itemsTimeArray = [], fromTime, toTime) {
  let obj = {
    filled: false,
    inner: {},
    outer: []
  };

  for (let j = fromTime; j < toTime; j += 1) {
    obj.inner[j] = [];
    obj.inner[j].push({
      time: timeDiapasonByMinutes(j, 0, j + 1, 0),
      from: formatTime(j, 0),
      to: formatTime(j + 1, 0),
      type: FILL_ZONE_TYPES.unfilled,
      left: 0,
      width: 100,
      id: '0'
    });
  }

  itemsTimeArray.forEach(item => {
    const dividedItem = divideValuesByHours(item);

    obj.inner = getInnerCoordinates(dividedItem, obj.inner);
    obj.outer.push(getOuterCoordinates(item, fromTime, toTime));
  });
  obj.filled = isFilled(obj.inner);

  return obj;
}
