import {
  getLeftTimePosition,
  getCurrentTimeState,
  fillItemObject,
  getMobileScrollCoordinates,
  getMobileScrollCoordinateByID,
  resetOpacityScrollCoordinate,
  shiftScrollCoordinate
} from './timeline.model';

const OriginalDate = Date;

describe('Timeline Model test', () => {
  it('get left coordinate position for line', () => {
    let date = new Date();
    let thrownError;
    date.setHours(9);
    date.setMinutes(53);

    expect(getLeftTimePosition(date.getHours(), date.getMinutes(), 8, 10)).toBe(94.16666666666667);
    expect(getLeftTimePosition(date.getHours(), date.getMinutes(), 6, 12)).toBe(64.72222222222221);
    try {
      getLeftTimePosition(date.getHours(), date.getMinutes(), 10, 12);
    } catch (e) {
      thrownError = e;
    }
    try {
      getLeftTimePosition(date.getHours(), date.getMinutes(), 6, 8);
    } catch (e) {
      thrownError = e;
    }
    expect(thrownError.code).toBe(2);
    try {
      getLeftTimePosition(date.getHours(), date.getMinutes(), 9, 8);
    } catch (e) {
      thrownError = e;
    }
    expect(thrownError.code).toBe(2);
  });

  it('current time object state', () => {
    const spy = jest.spyOn(global, 'Date');
    Date.mockImplementation(() => new OriginalDate('Tue Oct 16 2018 15:49:11'));

    expect(getCurrentTimeState(11, 19).currentHour).toBe(15);
    expect(getCurrentTimeState(11, 19).leftTimePosition).toBe(60.20833333333333);
    expect(getCurrentTimeState(11, 19).currentTimeTitle).toBe('15:49');
    expect(getCurrentTimeState(11, 19).pastTimes[0]).toBe(11);
    expect(getCurrentTimeState(11, 19).pastTimes[1]).toBe(12);

    Date.mockImplementation(() => new OriginalDate('Tue Oct 16 2018 13:09:11'));
    expect(getCurrentTimeState(11, 19).currentHour).toBe(13);
    expect(getCurrentTimeState(11, 19).leftTimePosition).toBe(26.875);
    expect(getCurrentTimeState(11, 19).currentTimeTitle).toBe('13:09');
    expect(getCurrentTimeState(11, 19).pastTimes[0]).toBe(11);
    expect(getCurrentTimeState(11, 19).pastTimes[1]).toBe(12);
    expect(getCurrentTimeState(11, 13).pastTimes[2]).toBe(13);

    spy.mockRestore();
  });

  it('filling zone', () => {
    let filled = [
      {
        from: '09:00',
        to: '09:15',
        id: 1
      },
      {
        from: '09:45',
        to: '10:00',
        id: 2
      },
      {
        from: '12:10',
        to: '15:30',
        id: 3
      },
      {
        from: '20:00',
        to: '23:00',
        id: 4
      }
    ];
    let fillObj = fillItemObject(filled, 8, 23);

    expect(typeof fillObj).toBe('object');
    expect(typeof fillObj.inner).toBe('object');
    expect(fillObj.outer.length).toBe(4);
    expect(fillObj.filled).toBeFalsy();

    expect(fillObj.inner['8'].length).toBe(1);
    expect(fillObj.inner['9'].length).toBe(3);

    expect(fillObj.inner['8'][0].type).toBe('unfilled');
    expect(fillObj.inner['8'][0].time).toBe('08:00-09:00');
    expect(fillObj.inner['9'][0].type).toBe('filled');
    expect(fillObj.inner['9'][0].time).toBe('09:00-09:15');
    expect(fillObj.inner['9'][1].type).toBe('unfilled');
    expect(fillObj.inner['9'][1].time).toBe('09:15-09:45');
    expect(fillObj.inner['9'][2].type).toBe('filled');
    expect(fillObj.inner['9'][2].time).toBe('09:45-10:00');

    expect(fillObj.outer[0].id).toBe(1);
    expect(fillObj.outer[0].time).toBe('09:00-09:15');
    expect(fillObj.outer[1].time).toBe('09:45-10:00');
    expect(fillObj.outer[2].time).toBe('12:10-15:30');
    expect(fillObj.outer[3].time).toBe('20:00-23:00');

    filled = [
      {
        from: '08:00',
        to: '09:00',
        id: 1
      }
    ];
    fillObj = fillItemObject(filled, 8, 9);

    expect(fillObj.filled).toBeTruthy();
  });

  it('get mobile scroll coordinates', () => {
    const dataList = [{ id: 'header_1', main: [{ id: 'main_1' }, { id: 'main_2' }] }];

    expect(getMobileScrollCoordinates(dataList).length).toBe(3);
    expect(getMobileScrollCoordinates(dataList)[0].id).toBe('header_1');
    expect(getMobileScrollCoordinates(dataList)[1].left).toBe(0);
    expect(getMobileScrollCoordinates(dataList)[0].opacity).toBe(0);
  });

  it('get object scroll coordinates by id', () => {
    let scrollCoordinates = [
      { id: 'header_1', left: 0, opacity: 1 },
      { id: 'main_1', left: 100, opacity: 1 },
      { id: 'main_2', left: 0, opacity: 1 }
    ];

    expect(getMobileScrollCoordinateByID(scrollCoordinates, 'main_1').id).toBe('main_1');
    expect(getMobileScrollCoordinateByID(scrollCoordinates, 'main_1').left).toBe(100);
    expect(getMobileScrollCoordinateByID(scrollCoordinates, 'main_1').opacity).toBe(1);
  });

  it('reset all opacity to zero', () => {
    let scrollCoordinates = [
      { id: 'header_1', left: 0, opacity: 1 },
      { id: 'main_1', left: 100, opacity: 1 },
      { id: 'main_2', left: 0, opacity: 1 }
    ];

    expect(resetOpacityScrollCoordinate(scrollCoordinates)[0].opacity).toBe(0);
    expect(resetOpacityScrollCoordinate(scrollCoordinates)[1].opacity).toBe(0);
    expect(resetOpacityScrollCoordinate(scrollCoordinates)[2].opacity).toBe(0);
    expect(scrollCoordinates[2].opacity).toBe(1);
  });

  it('shift scroll coordinates of block to left', () => {
    let scrollCoordinates = [
      { id: 'header_1', left: 0, opacity: 1 },
      { id: 'main_1', left: 100, opacity: 1 },
      { id: 'main_2', left: 0, opacity: 1 }
    ];

    expect(shiftScrollCoordinate(scrollCoordinates, 10, 5, 0)[0].left).toBe('10px');
    expect(shiftScrollCoordinate(scrollCoordinates, 10, 5, 0)[0].opacity).toBe(1);
    expect(shiftScrollCoordinate(scrollCoordinates, 10, 50, 0)[0].left).toBe(0);
    expect(shiftScrollCoordinate(scrollCoordinates, 10, 50, 0)[0].opacity).toBe(0);
  });
});
