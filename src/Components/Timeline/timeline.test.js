import Timeline from './timeline';
import React from 'react';
import { shallow } from 'enzyme';

const list = [
  {
    id: 'header_1',
    type: 'header',
    contentMobileShift: '7 floor',
    content: '7 floor'
  },
  {
    id: 'main_1',
    type: 'main',
    contentMobileShift: 'Washing room',
    content: ['Washing room', '2 Employees'],
    filled: [
      ['09:00', '09:15'],
      ['09:45', '10:00'],
      ['11:05', '12:05'],
      ['12:10', '15:30'],
      ['20:00', '23:00']
    ]
  }
];

describe('Timeline', () => {
  beforeAll(() => {
    window.addEventListener = jest.fn();
    window.removeEventListener = jest.fn();
  });

  afterAll(() => {
    window.addEventListener.mockReset();
    window.removeEventListener.mockReset();
  });

  it('Timeline has time from 8 to 23 and one header and one main item list', () => {
    const wrapperShallow = shallow(
      <Timeline showCurrentTime={false} timeList={{ from: 8, to: 23 }} dataList={list} />
    );
    expect(wrapperShallow).toMatchSnapshot();
  });

  it("Timeline has time from 8 to 23 and hasn't list", () => {
    const wrapperShallow = shallow(
      <Timeline showCurrentTime={false} timeList={{ from: 8, to: 23 }} />
    );
    expect(wrapperShallow).toMatchSnapshot();
  });
});
