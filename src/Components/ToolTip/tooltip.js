import { Component } from 'react';
import ReactDOM from 'react-dom';
import React from 'react';
import { getTopCoordinate, getLeftCoordinate } from './tooltip.model';
import PropTypes from 'prop-types';
import './tooltip.scss';

let wrapperRef;
let innerWidth;
let innerHeight;

/**
 ToolTip container
 */
class ToolTip extends Component {
  /**
   component's constructor
   */
  constructor() {
    super();
    wrapperRef = React.createRef();
    innerWidth = window.innerWidth;
    innerHeight = window.innerHeight;
    this.state = {
      tooltipHeight: 0
    };
    this.resizeWindow = this.resizeWindow.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  /**
   componentDidMount life cycle method
   @returns {undefined}
   */
  async componentDidMount() {
    if (this.state.tooltipHeight !== wrapperRef.current.offsetHeight) {
      this.setState({
        tooltipHeight: wrapperRef.current.offsetHeight
      });
    }
    window.addEventListener('resize', this.resizeWindow);
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  /**
   componentWillUnmount life cycle method
   @return {undefined}
   */
  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeWindow);
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  /**
   resize window handler of the component
   @return {undefined}
   */
  resizeWindow() {
    if (this.props.onCloseComponent) {
      this.props.onCloseComponent();
    }
  }

  /**
   handle when user is clicked outside component
   @param {object} e - component's object
   @return {undefined}
   */
  handleClickOutside(e) {
    if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
      if (this.props.onCloseComponent()) {
        this.props.onCloseComponent();
      }
    }
  }

  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { domNode } = this.props;
    const rect = domNode.getBoundingClientRect();
    const offsetTargetHeight = domNode.offsetHeight;
    const boundingClientSize = {
      boundingClientTop: rect.top,
      boundingClientHeight: rect.height,
      boundingClientLeft: rect.left,
      boundingClientWidth: rect.width
    };

    const topCoordinate = getTopCoordinate(
      boundingClientSize.boundingClientTop,
      boundingClientSize.boundingClientHeight,
      offsetTargetHeight,
      innerHeight,
      this.state.tooltipHeight
    );
    const leftCoordinate = getLeftCoordinate(
      boundingClientSize.boundingClientLeft,
      boundingClientSize.boundingClientWidth,
      innerWidth
    );

    return ReactDOM.createPortal(
      <div
        ref={wrapperRef}
        className="tooltip-block"
        style={{
          top: topCoordinate ? topCoordinate.topComponent : 0,
          left: leftCoordinate ? leftCoordinate.leftComponent : 0
        }}
      >
        <div
          className={
            'arrow-tooltip ' +
            (topCoordinate && topCoordinate.type === 'top' ? 'arrow-top' : 'arrow-bottom')
          }
          style={{ left: leftCoordinate ? leftCoordinate.leftArrow : 0 }}
        />
        {this.props.children}
      </div>,
      domNode
    );
  }
}

ToolTip.defaultProps = {
  domNode: null,
  onCloseComponent: null
};

ToolTip.propTypes = {
  /** node of elements*/
  domNode: PropTypes.object.isRequired,
  /** close component*/
  onCloseComponent: PropTypes.func,
  /** children components*/
  children: PropTypes.object
};

export default ToolTip;
