const minWidthWindow = 320; //minimum size of window width when tooltip have to width 100%;
const sizeComponent = {
  heightArrow: 10, //height of arrow
  widthArrow: 20, //width of arrow
  radiusTooltip: 8, //tooltip radius
  sizeScroll: 20, //size of scroll
  tooltipWidth: 300, // tooltip width
  tooltipHeight: 30 // tooltip height
};

/**
 *
 * @param {number} boundingClientTop - top coordinate of the target element regarding a viewport
 * @param {number} boundingClientHeight - height of the target element regarding a viewport
 * @param {number} offsetTargetHeight - offset height of the target element
 * @param {number} innerWindowHeight - height of viewport
 * @param {number} tooltipHeight - tooltip height
 * @param {number} heightArrow - arrow height of the component
 * @param {number} sizeScroll - a scroll size
 * @return {{type: string, topComponent: number}} type - type of arrow (top, bottom);  topComponent - top coordinate
 */
export function getTopCoordinate(
  boundingClientTop,
  boundingClientHeight,
  offsetTargetHeight,
  innerWindowHeight,
  tooltipHeight = sizeComponent.tooltipHeight,
  heightArrow = sizeComponent.heightArrow,
  sizeScroll = sizeComponent.sizeScroll
) {
  const sizeTopObj = {
    type: 'top',
    topComponent: 0
  };

  const freeHeightAfterElem = innerWindowHeight - boundingClientTop - boundingClientHeight;
  //Define top or bottom visibility area and top's size
  if (freeHeightAfterElem < tooltipHeight + sizeScroll) {
    sizeTopObj.type = 'bottom';
    sizeTopObj.topComponent = -tooltipHeight - heightArrow;
  } else {
    sizeTopObj.type = 'top';
    sizeTopObj.topComponent = offsetTargetHeight + heightArrow;
  }

  return sizeTopObj;
}

/**
 *
 * @param {number} boundingClientLeft - left coordinate of the target element regarding a viewport
 * @param {number} boundingClientWidth - weight of the target element regarding a viewport
 * @param {number} innerWindowWidth - a width of a viewport
 * @param {number} widthArrow - arrow width
 * @param {number} radiusTooltip - tooltip radius
 * @return {{leftArrow: number, leftComponent: number}} leftArrow - left coordinate of the arrow; leftComponent - left coordinate of the component
 */
export function getLeftCoordinate(
  boundingClientLeft,
  boundingClientWidth,
  innerWindowWidth,
  widthArrow = sizeComponent.widthArrow,
  radiusTooltip = sizeComponent.radiusTooltip
) {
  const tooltipWidth =
    innerWindowWidth <= minWidthWindow ? minWidthWindow : sizeComponent.tooltipWidth;
  let middleElem = 0;
  let offsetWidthElem = 0;
  const sizeLeftObj = {
    leftArrow: 0,
    leftComponent: 0
  };

  //Define middle visibility area
  if (boundingClientLeft < 0 && innerWindowWidth < boundingClientWidth) {
    //both edges are hidden
    middleElem = Math.abs(boundingClientLeft) + innerWindowWidth / 2;
  } else if (boundingClientLeft < 0) {
    //left edge is hidden
    middleElem =
      Math.abs(boundingClientLeft) + (boundingClientWidth - Math.abs(boundingClientLeft)) / 2;
  } else if (innerWindowWidth - boundingClientLeft < boundingClientWidth) {
    //right edge is hidden
    middleElem = (innerWindowWidth - boundingClientLeft) / 2;
  } else {
    middleElem = boundingClientWidth / 2;
  }

  //Shift tooltip left or right
  if (
    boundingClientLeft + middleElem + tooltipWidth / 2 < innerWindowWidth &&
    boundingClientLeft + middleElem - tooltipWidth / 2 > 0
  ) {
    sizeLeftObj.leftComponent = middleElem - tooltipWidth / 2;
    sizeLeftObj.leftArrow = tooltipWidth / 2 - widthArrow / 2;
    //Shift tooltip right
  } else if (boundingClientLeft + middleElem + tooltipWidth / 2 >= innerWindowWidth) {
    if (
      tooltipWidth - (innerWindowWidth - boundingClientLeft) >
      tooltipWidth - widthArrow - radiusTooltip / 2
    ) {
      offsetWidthElem =
        tooltipWidth - (innerWindowWidth - boundingClientLeft) - widthArrow - radiusTooltip / 2;
      sizeLeftObj.leftArrow = offsetWidthElem + middleElem - widthArrow / 2;
    } else {
      offsetWidthElem = tooltipWidth - (innerWindowWidth - boundingClientLeft);
      sizeLeftObj.leftArrow = offsetWidthElem + middleElem - widthArrow / 2;
    }
    sizeLeftObj.leftComponent = -offsetWidthElem;
  }
  //Shift tooltip left
  else if (boundingClientLeft + middleElem - tooltipWidth / 2 <= 0) {
    if (
      boundingClientLeft < 0 &&
      boundingClientWidth - Math.abs(boundingClientLeft) <= widthArrow + radiusTooltip / 2
    ) {
      offsetWidthElem = boundingClientLeft + (widthArrow - radiusTooltip / 2);
      sizeLeftObj.leftArrow = offsetWidthElem + middleElem - widthArrow / 2;
    } else {
      offsetWidthElem = boundingClientLeft;
      sizeLeftObj.leftArrow = offsetWidthElem + middleElem - widthArrow / 2;
    }
    sizeLeftObj.leftComponent = -offsetWidthElem;
  }

  return sizeLeftObj;
}
