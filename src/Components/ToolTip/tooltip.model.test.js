import { getLeftCoordinate, getTopCoordinate } from './tooltip.model';

describe('Tooltip Model test', () => {
  it('getLeftCoordinate function', () => {
    //left edge is hidden
    expect(getLeftCoordinate(-100, 123, 400).leftArrow).toBe(17.5);
    expect(getLeftCoordinate(-100, 123, 400).leftComponent).toBe(84);
    //both edges are showed
    expect(getLeftCoordinate(100, 123, 400).leftArrow).toBe(140);
    expect(getLeftCoordinate(100, 123, 400).leftComponent).toBe(-88.5);
    //right edge is hidden
    expect(getLeftCoordinate(350, 123, 400).leftArrow).toBe(265);
    expect(getLeftCoordinate(350, 123, 400).leftComponent).toBe(-250);
    //both edges is hidden
    expect(getLeftCoordinate(-100, 500, 400).leftArrow).toBe(140);
    expect(getLeftCoordinate(-100, 500, 400).leftComponent).toBe(150);
  });

  it('getTopCoordinate function', () => {
    //top arrow
    expect(getTopCoordinate(199, 52, 52, 320, 30).type).toBe('top');
    expect(getTopCoordinate(199, 52, 52, 320, 30).topComponent).toBe(62);
    //bottom arrow
    expect(getTopCoordinate(305, 52, 52, 320, 30).type).toBe('bottom');
    expect(getTopCoordinate(305, 52, 52, 320, 30).topComponent).toBe(-40);
    //bottom arrow
    expect(getTopCoordinate(453, 52, 52, 320, 30).type).toBe('bottom');
    expect(getTopCoordinate(453, 52, 52, 320, 30).topComponent).toBe(-40);
  });
});
