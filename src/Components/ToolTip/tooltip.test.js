import ToolTip from './tooltip';
import React from 'react';
import { shallow } from 'enzyme';

const domNode = global.document.createElement('div');

describe('ToolTip', () => {
  it('ToolTip with input date', () => {
    const wrapperShallow = shallow(
      <ToolTip
        boundingClientSize={{
          boundingClientTop: 100,
          boundingClientHeight: 30,
          boundingClientLeft: -130,
          boundingClientWidth: 200
        }}
        offsetTargetHeight={20}
        domNode={domNode}
      >
        <div>test!</div>
      </ToolTip>
    );
    expect(wrapperShallow).toMatchSnapshot();
  });
});
