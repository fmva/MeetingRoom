import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import ToolTip from '../ToolTip/tooltip';
import CircleButton from '../CircleButton/circlebutton';
import '../Styles/styles.scss';
import './tooltip.time.info.scss';

/**
 TooltipTimeInfo container
 */
class TooltipTimeInfo extends Component {
  /**
   render component
   @return {Object} JSX object
   */
  render() {
    const {
      domNode,
      onCloseComponent,
      title,
      date,
      room,
      person,
      countPersons,
      imgSrc
    } = this.props;

    return (
      <ToolTip domNode={domNode} onCloseComponent={onCloseComponent}>
        <div>
          <div className="btn-update">
            <CircleButton type={'update'} classColor={'update-button-color'} />
          </div>
          <div className="tooltip-title-meeting">
            <span>{title}</span>
          </div>
          <div className="tooltip-date-meeting">
            <span>{date}</span>
            <span className="dot-before">{room}</span>
          </div>
          <div className="tooltip-members-meeting">
            <div className="tooltip-members-meeting-inner">
              {imgSrc && <img className="img-circle" src={imgSrc} />}
              <div className="tooltip-name-members">{person}</div>
            </div>
            <div className="tooltip-other-members">+{countPersons}</div>
          </div>
        </div>
      </ToolTip>
    );
  }
}

TooltipTimeInfo.defaultProps = {
  domNode: null,
  onCloseComponent: null,
  title: null,
  date: null,
  room: null,
  person: null,
  countPersons: null,
  imgSrc: null
};

TooltipTimeInfo.propTypes = {
  /** node of elements*/
  domNode: PropTypes.object.isRequired,
  /** close component*/
  onCloseComponent: PropTypes.func,
  /** title of meeting*/
  title: PropTypes.string,
  /** date of meeting*/
  date: PropTypes.string,
  /** title of meeting room*/
  room: PropTypes.string,
  /** person of meeting room*/
  person: PropTypes.string,
  /** person counts of meeting room*/
  countPersons: PropTypes.number,
  /** image path of the person*/
  imgSrc: PropTypes.string
};

export default TooltipTimeInfo;
