import TooltipTimeInfo from './tooltip.time.info';
import React from 'react';
import { shallow } from 'enzyme';

describe('TooltipTimeInfo', () => {
  it('TooltipTimeInfo with input date', () => {
    const wrapperShallow = shallow(
      <TooltipTimeInfo
        title={'title'}
        date={'date'}
        room={'room'}
        person={'person'}
        countPersons={'countPersons'}
        imgSrc={'/imgSrc'}
      />
    );
    expect(wrapperShallow).toMatchSnapshot();
  });
});
