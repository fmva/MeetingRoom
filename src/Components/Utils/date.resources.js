const months = {
  '0': 'January',
  '1': 'February',
  '2': 'March',
  '3': 'April',
  '4': 'May',
  '5': 'June',
  '6': 'July',
  '7': 'August',
  '8': 'September',
  '9': 'October',
  '10': 'November',
  '11': 'December'
};

/**
 * get title of month by nomber
 * @param {string} num - number of month
 * @return {string}  title of month
 */
export function getMonthsByNum(num) {
  return months[num];
}
