import { Component } from 'react';
import React from 'react';
import iconLoad from './icon-load.svg';
import './wait.scss';
import PropTypes from 'prop-types';

/**
 Wait container
 */
class Wait extends Component {
  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { isShow } = this.props;
    return (
      <div className="wait-box" style={{ display: isShow ? 'block' : 'none' }}>
        <div className="wait-box-inner">
          <img className="icon-wait" src={iconLoad} />
        </div>
      </div>
    );
  }
}

Wait.defaultProps = {
  isShow: false
};

Wait.propTypes = {
  /** show/hide wait component */
  isShow: PropTypes.bool
};

export default Wait;
