import Wait from './wait';
import React from 'react';
import { shallow } from 'enzyme';

describe('Wait', () => {
  it('Wait component render', () => {
    const wrapperShallow = shallow(<Wait />);
    expect(wrapperShallow).toMatchSnapshot();
  });
});
