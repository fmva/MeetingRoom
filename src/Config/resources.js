const list = {
  button_add_meeting: 'Add',
  button_success: 'OK'
};

const Resources = {
  /**
   * get resource
   * @param {String} title - title of resource
   * @return {String} - resource
   */
  getResource: function(title) {
    return list[title] ? list[title] : `?${title}?`;
  }
};

export default Resources;
