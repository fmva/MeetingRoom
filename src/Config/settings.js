const params = {
  editFormPath: 'editform/:date?',
  mainPath: 'main/:date?',
  server_host: 'localhost',
  server_port: '5555',
  protocol: 'http'
};

/**
 * get settings
 * @param {String} value - name of params
 * @return {object} - value of params
 */
export function getSettings(value) {
  return params[value] ? params[value] : null;
}

/**
 * get full host name
 * @return {object} - json
 */
export function getHost() {
  return `${params.protocol}://${params.server_host}:${params.server_port}`;
}
