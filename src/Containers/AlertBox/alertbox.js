import { Component } from 'react';
import React from 'react';
import { connect } from 'react-redux';
import AlertBox from '../../Components/AlertBox/alertbox';
import Selectors from '../../Store/AlertBox/selectors';
import PropTypes from 'prop-types';
import { hideAlert } from '../../Store/AlertBox/actions';

/**
 AlertBox container
 */
class AlertBoxContainer extends Component {
  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const { headerText, type, hideAlert, titleButton } = this.props;

    if (type === 'none') {
      return null;
    }

    return (
      <AlertBox
        headerText={headerText}
        type={type}
        onCloseClick={type === 'success' ? () => hideAlert() : null}
        tittleButton={titleButton}
      />
    );
  }
}

AlertBoxContainer.defaultProps = {
  headerText: '',
  hideAlert: null,
  type: 'none'
};

AlertBoxContainer.propTypes = {
  /** header text */
  headerText: PropTypes.string,
  /** type of alert*/
  type: PropTypes.oneOf(['none', 'error', 'success']),
  /** click close event*/
  hideAlert: PropTypes.func,
  /** title button*/
  titleButton: PropTypes.string
};

/**
 connecting function with state
 @param {object} state -  state application
 @returns {Object} state
 */
function mapStateToProps(state) {
  return {
    headerText: Selectors.getAlertHeaderText(state),
    type: Selectors.getType(state)
  };
}

const mapDispatchToProps = {
  hideAlert
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AlertBoxContainer);
