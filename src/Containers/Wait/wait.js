import { Component } from 'react';
import React from 'react';
import { connect } from 'react-redux';
import Wait from '../../Components/Wait/wait';
import Selectors from '../../Store/Wait/selectors';
import PropTypes from 'prop-types';

/**
 Wait container
 */
class WaitContainer extends Component {
  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    console.log('isShow', this.props);
    return <Wait isShow={this.props.isWait} />;
  }
}

WaitContainer.propTypes = {
  /** show/hide wait component */
  isWait: PropTypes.bool
};

/**
 connecting function with state
 @param {object} state -  state application
 @returns {Object} state
 */
function mapStateToProps(state) {
  console.log('WaitContainer', state);
  return {
    isWait: Selectors.getWait(state.wait)
  };
}

export default connect(mapStateToProps)(WaitContainer);
