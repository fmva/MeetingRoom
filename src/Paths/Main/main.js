import { Component } from 'react';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Header from '../../Components/Header/header';
import DateSwitch from '../../Components/Dateswitch/dateswitch';
import Button from '../../Components/Button/button';
import Timeline from '../../Components/Timeline/timeline';
import Resources from '../../Config/resources';
import WaitContainer from '../../Containers/Wait/wait';
import {
  fetchTimeline,
  setCurrentDate,
  setToolTipNode,
  fetchTimeLineAndRooms
} from '../../Store/Main/actions';
import { fetchDetails } from '../../Store/Details/actions';
import Selectors from '../../Store/Main/selectors';
import detailsSelectors from '../../Store/Details/selectors';
import roomsSelectors from '../../Store/Rooms/selectors';
import './main.scss';

import AlertBoxContainer from '../../Containers/AlertBox/alertbox';
import TooltipTimeInfo from '../../Components/TooltipTimeInfo/tooltip.time.info';
import { openErrorAlert } from '../../Store/AlertBox/actions';
import { showWait, hideWait } from '../../Store/Wait/actions';

import { addOneDay, invertStringDate, subtractOneDay, getDateFromString } from '../../Utils/date';
import { getRoomById } from '../../Utils/rooms';

import { getHost } from '../../Config/settings';

import PropTypes from 'prop-types';

let toolTipNode = null; //
/**
 Main container
 */
class Main extends Component {
  /**
   component's constructor
   */
  constructor() {
    super();
  }

  /**
   componentDidMount life cycle method
   @returns {undefined}
   */
  async componentDidMount() {
    console.log('main props', this.props);
    let date = this.props.date;
    if (this.props.match.params && this.props.match.params.date) {
      try {
        date = getDateFromString(this.props.match.params.date);
      } catch (e) {
        this.props.openErrorAlert(e);
        return;
      }
      this.props.setCurrentDate(date);
    }
    this.fetchTimeLine(date);
  }

  /**
   render main list content
   @param {string[]} titles - titles for shift
   @returns {Object} JSX object
   */
  renderTimeLineListMainContent(titles) {
    let title = titles.length > 0 ? titles[0] : '',
      count = titles.length > 1 ? titles[1] : '';

    return (
      <React.Fragment>
        <div className="title-meeting">
          <span className="title-meeting-text">{title}</span>
        </div>
        <div className="count-people">
          <span className="count-people-text">{count}</span>
        </div>
      </React.Fragment>
    );
  }

  /**
   fetch timeline data
   @param {object} date - date
   @returns {undefined}
   */
  async fetchTimeLine(date) {
    let jsonTimeline, jsonRooms;
    this.props.showWait();

    if (this.props.rooms.length === 0) {
      [jsonTimeline, jsonRooms] = await this.props.fetchTimeLineAndRooms(date);

      if (this.showErrorStatus(jsonRooms)) {
        return;
      }
    } else {
      jsonTimeline = await this.props.fetchTimeline(date, this.props.rooms);
    }

    if (this.showErrorStatus(jsonTimeline)) {
      return;
    }

    this.props.hideWait();
  }

  /**
   * show error text if it is existed
   * @param {object} json  - json with error text
   * @return {boolean} - return status error
   */
  showErrorStatus(json) {
    if (json.error.status) {
      this.props.openErrorAlert(json.error.text);
      this.props.hideWait();
      return true;
    }

    return false;
  }

  /**
   fetch data details of the meeting
   * @param {object} target - dom of the tooltip
   * @param {string} id - id of the meeting
   * @param {string} timeFrom - beginning time
   * @param {string} timeTo - ending time
   * @param {Date} date - date of the meeting
   * @return {undefined}
   */
  async fetchDetails(target, id, timeFrom, timeTo, date) {
    this.props.showWait();
    const json = await this.props.fetchDetails(id, timeFrom, timeTo, date);
    if (json.error.status) {
      this.props.openErrorAlert(json.error.text);
    } else {
      toolTipNode = target;
      this.props.setToolTipNode(true);
    }
    this.props.hideWait();
  }

  /**
   increase date on one day
   @returns {undefined}
   */
  async increaseOneDay() {
    const increasedDate = addOneDay(this.props.date);
    this.props.setCurrentDate(increasedDate);
    await this.fetchTimeLine(increasedDate);
  }

  /**
   subtract one day from date
   @returns {undefined}
   */
  async subtractOneDay() {
    const increasedDate = subtractOneDay(this.props.date);
    this.props.setCurrentDate(increasedDate);
    await this.fetchTimeLine(increasedDate);
  }

  /**
   render component
   @returns {Object} JSX object
   */
  render() {
    const room = getRoomById(this.props.rooms, this.props.details.id_meeting);

    return (
      <React.Fragment>
        <header>
          <Header
            isBottomBorder={false}
            rightContent={<Button text={Resources.getResource('button_add_meeting')} />}
          />
        </header>
        <div className="is--devices date-switch-block-device">
          <DateSwitch
            date={this.props.date}
            onClickLeftButton={async () => {
              await this.subtractOneDay();
            }}
            onClickRightButton={async () => {
              await this.increaseOneDay();
            }}
            onClickCalendarDay={date => {
              this.props.setCurrentDate(date);
              this.fetchTimeLine(date);
            }}
          />
        </div>
        <main>
          {this.props.timeline && this.props.timeline.data && this.props.timeline.data.length > 0 && (
            <Timeline
              headerContent={
                <div className="is-desktop date-switch-wrapper">
                  <DateSwitch
                    date={this.props.date}
                    onClickLeftButton={async () => {
                      await this.subtractOneDay();
                    }}
                    onClickRightButton={async () => {
                      await this.increaseOneDay();
                    }}
                    onClickCalendarDay={date => {
                      this.props.setCurrentDate(date);
                      this.fetchTimeLine(date);
                    }}
                  />
                </div>
              }
              showCurrentTime={true}
              timeList={{ from: 8, to: 23 }}
              dataList={this.props.timeline}
              listMainContent={this.renderTimeLineListMainContent}
              onClickFilledZone={(e, id, from, to) => {
                this.fetchDetails(e.target, id, from, to, this.props.date);
              }}
            />
          )}

          <WaitContainer />

          <AlertBoxContainer titleButton={Resources.getResource('button_success')} />
          {this.props.isToolTipNode && (
            <TooltipTimeInfo
              domNode={toolTipNode}
              title={this.props.details.title}
              date={`${invertStringDate(this.props.details.date)}, ${this.props.details.from} - ${
                this.props.details.to
              }`}
              room={room.title ? room.title : ''}
              person={`${this.props.details.persons[0].img} ${this.props.details.persons[0].fam}`}
              imgSrc={getHost() + this.props.details.persons[0].personImg}
              countPersons={this.props.details.persons.length - 1}
              onCloseComponent={() => {
                this.props.setToolTipNode(null);
              }}
            />
          )}
        </main>
      </React.Fragment>
    );
  }
}

Main.propTypes = {
  /** current selected date */
  date: PropTypes.instanceOf(Date),
  /** all rooms data*/
  rooms: PropTypes.array,
  /** details of the meeting */
  details: PropTypes.object,
  /** tooltip object */
  isToolTipNode: PropTypes.bool,
  /** timeline  data */
  timeline: PropTypes.object,
  /** fetch timeline and rooms data from server */
  fetchTimeLineAndRooms: PropTypes.func,
  /** fetch timeline data from server */
  fetchTimeline: PropTypes.func,
  /** fetch timeline data from server */
  fetchDetails: PropTypes.func,
  /** set tooltip a dom node to state*/
  setToolTipNode: PropTypes.func,
  /** show wait window */
  showWait: PropTypes.func,
  /** hide wait window */
  hideWait: PropTypes.func,
  /** show error message */
  openErrorAlert: PropTypes.func,
  /** set current date */
  setCurrentDate: PropTypes.func,
  /** url match object */
  match: PropTypes.object
};

/**
 connecting function with state
 @param {object} state -  state application
 @returns {Object} state
 */
function mapStateToProps(state) {
  return {
    date: Selectors.getSelectedDate(state),
    timeline: Selectors.getTimeline(state),
    details: detailsSelectors.getDetails(state),
    isToolTipNode: Selectors.getToolTipNode(state),
    rooms: roomsSelectors.getRooms(state)
  };
}

const mapDispatchToProps = {
  setToolTipNode,
  fetchTimeline,
  fetchDetails,
  fetchTimeLineAndRooms,
  setCurrentDate,
  showWait,
  hideWait,
  openErrorAlert
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Main)
);
