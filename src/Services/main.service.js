import Server from '../Utils/server';
import { getFormattedDate } from '../Utils/date';
import { getHost } from '../Config/settings';

const urls = {
  timelineUrl: `${getHost()}/timeline/:date/`,
  detailsUrl: `${getHost()}/details/:date/:id/?from=:from&to=:to`,
  roomsUrl: `${getHost()}/rooms/`
};

/**
 * get timeline json from server
 * @param {Date} date - date
 * @return {object} - json
 */
export async function getTimelineData(date = new Date()) {
  return await Server.fetch(urls.timelineUrl.replace(':date', getFormattedDate(date)));
}

/**
 * get details of meeting
 * @param {string} id - id of meeting room
 * @param {string} timeFrom - from this time
 * @param {string} timeTo - into this time
 * @param {Date} date - date
 * @return {object} - json
 */
export async function getDetailsMeeting(id, timeFrom, timeTo, date = new Date()) {
  return await Server.fetch(
    urls.detailsUrl
      .replace(':date', getFormattedDate(date))
      .replace(':id', id)
      .replace(':from', timeFrom)
      .replace(':to', timeTo)
  );
}

/**
 * get timeline json from server and rooms
 * @param {Date} date - date
 * @return {object} - json
 */
export async function getTimeLineAndRooms(date = new Date()) {
  return await Server.fetchAll([
    urls.timelineUrl.replace(':date', getFormattedDate(date)),
    urls.roomsUrl
  ]);
}

/**
 * get rooms
 * @return {object} - json
 */
/*export async function getRooms() {
  return await Server.fetch(urls.roomsUrl);
}*/
