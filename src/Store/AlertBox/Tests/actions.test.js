import { hideAlert, openErrorAlert } from '../actions';

describe('AlertBox Actions', () => {
  it('hideAlert action', async () => {
    const result = hideAlert();

    expect(result.type).toBe('CLOSE_ALERT');
    expect(result.messageModal.type).toBe('none');
    expect(result.messageModal.textHeader).toBeNull();
    expect(result.messageModal.textContent).toBeNull();
  });

  it('openErrorAlert action', async () => {
    const result = openErrorAlert('Text');

    expect(result.type).toBe('OPEN_ERROR_ALERT');
    expect(result.messageModal.type).toBe('error');
    expect(result.messageModal.textHeader).toBe('Text');
    expect(result.messageModal.textContent).toBeNull();
  });
});
