import reduce from '../reducer';

describe('AlertBox reducer', () => {
  it('default AlertBox state', () => {
    const result = reduce();

    expect(result.type).toBe('none');
    expect(result.textHeader).toBeNull();
    expect(result.textContent).toBeNull();
  });

  it('CLOSE_ALERT action', () => {
    const result = reduce(undefined, {
      type: 'CLOSE_ALERT',
      messageModal: { type: 'none', textHeader: null, textContent: null }
    });

    expect(result.type).toBe('none');
    expect(result.textHeader).toBeNull();
    expect(result.textContent).toBeNull();
  });

  it('OPEN_ERROR_ALERT action', () => {
    const result = reduce(undefined, {
      type: 'OPEN_ERROR_ALERT',
      messageModal: { type: 'error', textHeader: 'text', textContent: null }
    });

    expect(result.type).toBe('error');
    expect(result.textHeader).toBe('text');
    expect(result.textContent).toBeNull();
  });
});
