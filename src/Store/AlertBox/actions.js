import * as types from './action.types';

/**
 * hide alertBox component
 * @return {object} - return action object
 */
export function hideAlert() {
  return {
    type: types.CLOSE_ALERT,
    messageModal: {
      type: 'none',
      textHeader: null,
      textContent: null
    }
  };
}

/**
 * Open error alert
 * @param {string} textError - text error
 * @return {object} - return action object
 */
export function openErrorAlert(textError) {
  return {
    type: types.OPEN_ERROR_ALERT,
    messageModal: {
      type: 'error',
      textHeader: textError,
      textContent: null
    }
  };
}
