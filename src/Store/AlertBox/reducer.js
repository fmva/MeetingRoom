import Immutable from 'seamless-immutable';
import * as types from './action.types';

const initialState = Immutable({
  type: 'none',
  textHeader: null,
  textContent: null
});

/**
 * reducer of AlertBox container
 * @param {object} state - state from action
 * @param {object} action - action
 * @return {object} - new state
 */
export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.CLOSE_ALERT:
    case types.OPEN_ERROR_ALERT:
      return state.merge(action.messageModal);
    default:
      return state;
  }
}
