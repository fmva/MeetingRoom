/**
 * get header text of alert
 * @param {object} state - global state
 * @return {string} - return text
 * */
function getAlertHeaderText(state) {
  return state.alertBox.textHeader;
}

/**
 * get type of alert
 * @param {object} state - global state
 * @return {string} - type of alert
 * */
function getType(state) {
  return state.alertBox.type;
}

export default {
  getAlertHeaderText: getAlertHeaderText,
  getType: getType
};
