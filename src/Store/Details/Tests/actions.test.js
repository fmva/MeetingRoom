import { fetchDetails } from '../actions';
import { getDetailsMeeting } from '../../../Services/main.service';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

jest.mock('../../../Services/main.service');

const errorAnswer = { error: { status: true, text: 'Error' } };
const dataAnswer = { data: [], error: { status: false, text: '' } };

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Details Actions', () => {
  it('fetchDetails error response', async () => {
    getDetailsMeeting.mockResolvedValue(errorAnswer);

    const store = mockStore({});
    await store.dispatch(fetchDetails('main_1', '10;00', '11:00', new Date()));

    expect(store.getActions()[0].type).toBe('DETAILS_ERROR_FETCHED');
    expect(store.getActions()[0].json.error.status).toBeTruthy();
    expect(store.getActions()[0].json.error.text).toBe('Error');
  });

  it('fetchDetails success response', async () => {
    getDetailsMeeting.mockResolvedValue(dataAnswer);

    const store = mockStore({});
    await store.dispatch(fetchDetails('main_1', '10;00', '11:00', new Date()));

    expect(store.getActions()[0].type).toBe('DETAILS_SUCCESS_FETCHED');
    expect(store.getActions()[0].json.data.length).toBe(0);
    expect(store.getActions()[0].json.error.status).toBeFalsy();
    expect(store.getActions()[0].json.error.text).toBe('');
  });
});
