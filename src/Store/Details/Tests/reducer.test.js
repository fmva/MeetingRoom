import reduce from '../reducer';

describe('Details reducer', () => {
  it('DETAILS_SUCCESS_FETCHED action', () => {
    const result = reduce(undefined, {
      type: 'DETAILS_SUCCESS_FETCHED',
      json: {
        data: {
          id: 100,
          id_meeting: 'main_3',
          title: 'Long discussion',
          date: '2019-06-12',
          from: '10:30',
          to: '11:30',
          person: 'Dart Wader',
          countPersons: 17,
          personImg: '/UserImg/1819650.jpg'
        }
      }
    });

    expect(result.details.id).toBe(100);
    expect(result.details.id_meeting).toBe('main_3');
    expect(result.details.title).toBe('Long discussion');
    expect(result.details.date).toBe('2019-06-12');
    expect(result.details.from).toBe('10:30');
    expect(result.details.to).toBe('11:30');
    expect(result.details.person).toBe('Dart Wader');
    expect(result.details.countPersons).toBe(17);
    expect(result.details.personImg).toBe('/UserImg/1819650.jpg');
  });

  it('DETAILS_ERROR_FETCHED action', () => {
    const result = reduce(undefined, {
      type: 'DETAILS_ERROR_FETCHED',
      json: { error: { status: true } }
    });
    expect(result.details).toEqual({});
  });
});
