import { getDetailsMeeting } from '../../Services/main.service';
import * as types from './action.types';

/**
 * get details of the meeting from server
 * @param {string} id - id of the meeting
 * @param {string} timeFrom - beginning time
 * @param {string} timeTo - ending time
 * @param {Date} date - date of the meeting
 * @return {function} - return async function
 */
export function fetchDetails(id, timeFrom, timeTo, date) {
  return async dispatch => {
    const json = await getDetailsMeeting(id, timeFrom, timeTo, date);
    if (json.error.status) {
      dispatch({ type: types.DETAILS_ERROR_FETCHED, json });
    } else {
      dispatch({ type: types.DETAILS_SUCCESS_FETCHED, json });
    }
    return json;
  };
}
