import Immutable from 'seamless-immutable';
import * as types from './action.types';

const initialState = Immutable({
  details: {}
});

/**
 * reducer of Details
 * @param {object} state - state from action
 * @param {object} action - action
 * @return {object} - new state
 */
export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.DETAILS_SUCCESS_FETCHED:
      return state.merge({
        details: action.json.data
      });
    case types.DETAILS_ERROR_FETCHED:
      return state.merge({
        details: {}
      });
    default:
      return state;
  }
}
