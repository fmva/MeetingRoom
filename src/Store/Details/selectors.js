/**
 * get details
 * @param {object} state - global state
 * @return {object} - return date parameter
 * */
function getDetails(state) {
  return state.details.details;
}

export default {
  getDetails: getDetails
};
