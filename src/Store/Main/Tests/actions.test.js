import { fetchTimeline, setCurrentDate, setToolTipNode, fetchTimeLineAndRooms } from '../actions';
import { getTimeLineAndRooms, getTimelineData } from '../../../Services/main.service';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

jest.mock('../../../Services/main.service');

const errorAnswer = { error: { status: true, text: 'Error' } };
const dataAnswer = { data: [], error: { status: false, text: '' } };
const answerTimeLineAndRooms = [dataAnswer, dataAnswer];

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Main Actions', () => {
  it('fetchTimeline error response', async () => {
    getTimelineData.mockResolvedValue(errorAnswer);

    const store = mockStore({});
    await store.dispatch(fetchTimeline(new Date(), []));

    expect(store.getActions()[0].type).toBe('TIMELINE_ERROR_FETCHED');
    expect(store.getActions()[0].json.timeline.error.status).toBeTruthy();
    expect(store.getActions()[0].json.timeline.error.text).toBe('Error');
  });

  it('fetchTimeline success response', async () => {
    getTimelineData.mockResolvedValue(dataAnswer);

    const store = mockStore({});
    await store.dispatch(fetchTimeline(new Date(), []));

    expect(store.getActions()[0].type).toBe('TIMELINE_SUCCESS_FETCHED');
    expect(store.getActions()[0].json.timeline.data.length).toBe(0);
    expect(store.getActions()[0].json.timeline.error.status).toBeFalsy();
    expect(store.getActions()[0].json.timeline.error.text).toBe('');
    expect(store.getActions()[0].json.rooms.length).toBe(0);
  });

  it('fetchTimeLineAndRooms success response', async () => {
    getTimeLineAndRooms.mockResolvedValue(answerTimeLineAndRooms);

    const store = mockStore({});
    await store.dispatch(fetchTimeLineAndRooms(new Date()));

    expect(store.getActions()[0].type).toBe('ROOMS_SUCCESS_FETCHED');
    expect(store.getActions()[1].type).toBe('TIMELINE_SUCCESS_FETCHED');

    expect(store.getActions()[0].json.data.length).toBe(0);
    expect(store.getActions()[1].json.timeline.data.length).toBe(0);
    expect(store.getActions()[1].json.timeline.error.status).toBeFalsy();
    expect(store.getActions()[1].json.timeline.error.text).toBe('');
  });

  it('increaseOneDay action', async () => {
    const result = setCurrentDate(new Date('December 17, 2019 23:15:30'));

    expect(result.type).toBe('SET_CURRENT_DATE');
    expect(result.date.getDate()).toBe(17);
    expect(result.date.getMonth() + 1).toBe(12);
    expect(result.date.getFullYear()).toBe(2019);
  });

  it('setToolTipNode action', async () => {
    const result = setToolTipNode(true);

    expect(result.type).toBe('SET_TOOLTIP_NODE');
    expect(result.node).toBeTruthy();

    const resultRemove = setToolTipNode(false);
    expect(resultRemove.type).toBe('REMOVE_TOOLTIP_NODE');
    expect(resultRemove.node).toBeFalsy();
  });
});
