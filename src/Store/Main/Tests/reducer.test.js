import reduce from '../reducer';

const successData = {
  type: 'TIMELINE_SUCCESS_FETCHED',
  json: {
    rooms: [
      {
        id: 1,
        title: 'Washing room',
        amount: 'up to 10 employees',
        floor: 7
      },
      {
        id: 2,
        title: 'Yellow Boat',
        amount: '3 - 6 employees',
        floor: 7
      }
    ],
    timeline: {
      data: [
        {
          id: 1,
          floor: '7 floor',
          main: [
            {
              id: 1,
              filled: [
                {
                  from: '09:00',
                  to: '09:05',
                  id: 1
                },
                {
                  from: '09:10',
                  to: '09:15',
                  id: 2
                },
                {
                  from: '09:30',
                  to: '11:30',
                  id: 3
                },
                {
                  from: '12:10',
                  to: '18:58',
                  id: 4
                }
              ]
            },
            {
              id: 2,
              filled: [
                {
                  from: '08:00',
                  to: '23:00',
                  id: 5
                }
              ]
            }
          ]
        }
      ]
    }
  }
};
const errorData = {
  type: 'TIMELINE_ERROR_FETCHED',
  json: { error: { status: true } }
};

describe('Main reducer', () => {
  it('TIMELINE_SUCCESS_FETCHED action', () => {
    const result = reduce(undefined, successData);
    expect(result.timeline.data.length).toBe(1);
    expect(result.timeline.data[0].main.length).toBe(2);
    expect(result.timeline.data[0].contentMobileShift).toBe('7 floor');
    expect(result.timeline.data[0].content).toBe('7 floor');

    expect(result.timeline.data[0].main[0].contentMobileShift).toBe('Washing room');
    expect(result.timeline.data[0].main[0].content[0]).toBe('Washing room');
    expect(result.timeline.data[0].main[0].content[1]).toBe('up to 10 employees');

    expect(result.timeline.data[0].main[1].contentMobileShift).toBe('Yellow Boat');
    expect(result.timeline.data[0].main[1].content[0]).toBe('Yellow Boat');
    expect(result.timeline.data[0].main[1].content[1]).toBe('3 - 6 employees');
  });

  it('TIMELINE_ERROR_FETCHED action', () => {
    const result = reduce(undefined, errorData);
    expect(result.timeline).toEqual({});
  });

  it('INCREASE_ONE_DAY action', () => {
    const result = reduce(undefined, {
      type: 'SET_CURRENT_DATE',
      date: new Date('December 17, 2019 23:15:30')
    });
    expect(result.date.getDate()).toBe(17);
    expect(result.date.getMonth() + 1).toBe(12);
    expect(result.date.getFullYear()).toBe(2019);
  });

  it('SET_TOOLTIP_NODE and REMOVE_TOOLTIP_NODE actions', () => {
    const result = reduce(undefined, {
      type: 'SET_TOOLTIP_NODE',
      node: true
    });
    expect(result.toolTipNode).toBeTruthy();
  });
});
