import { getTimelineData, getTimeLineAndRooms } from '../../Services/main.service';
import * as types from './action.types';
import { dispatchRooms } from '../Rooms/actions';

/**
 * get timeline and rooms json from server
 * @param {Date} date - date
 * @return {function} - return async function
 */
export function fetchTimeLineAndRooms(date) {
  return async dispatch => {
    const json = await getTimeLineAndRooms(date);
    const [jsonTimeline, jsonRooms] = json;

    dispatchRooms(dispatch, jsonRooms);
    dispatchTimeline(dispatch, jsonTimeline, jsonRooms.data);
    return json;
  };
}

/**
 * dispatch timeline data
 * @param {function} dispatch - dispatch function
 * @param {object} jsonTimeline - timeline json
 * @param {object} jsonRooms - rooms json
 * @return {undefined}
 */
function dispatchTimeline(dispatch, jsonTimeline, jsonRooms) {
  if (jsonTimeline.error.status) {
    dispatch({
      type: types.TIMELINE_ERROR_FETCHED,
      json: { timeline: jsonTimeline, rooms: jsonRooms }
    });
  } else {
    dispatch({
      type: types.TIMELINE_SUCCESS_FETCHED,
      json: { timeline: jsonTimeline, rooms: jsonRooms }
    });
  }
}

/**
 * get timeline json from server
 * @param {Date} date - date
 * @param {object} jsonRooms - object of rooms
 * @return {function} - return async function
 */
export function fetchTimeline(date, jsonRooms) {
  return async dispatch => {
    const json = await getTimelineData(date);
    dispatchTimeline(dispatch, json, jsonRooms);
    return json;
  };
}

/**
 * set current date
 * @param {Date} date - date
 * @return {Object} - return action object
 */
export function setCurrentDate(date) {
  return {
    type: types.SET_CURRENT_DATE,
    date: date
  };
}

/**
 * set tooltip node
 * @param {boolean} node - dom node flag of the tooltip
 * @return {Object} - return action object
 */
export function setToolTipNode(node) {
  if (node) {
    return {
      type: types.SET_TOOLTIP_NODE,
      node: true
    };
  } else {
    return {
      type: types.REMOVE_TOOLTIP_NODE,
      node: false
    };
  }
}
