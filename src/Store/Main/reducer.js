import Immutable from 'seamless-immutable';
import * as types from './action.types';
import { getRoomById } from '../../Utils/rooms';

const initialState = Immutable({
  date: new Date(),
  timeline: {},
  toolTipNode: null
});

/**
 * reducer of Main container
 * @param {object} state - state from action
 * @param {object} action - action
 * @return {object} - new state
 */
export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.TIMELINE_SUCCESS_FETCHED:
      return state.merge({
        timeline: {
          timestamp: new Date().getTime(),
          data: action.json.timeline.data.map(item => {
            const main = item.main.map(itemMain => {
              const room = getRoomById(action.json.rooms, itemMain.id);
              return {
                id: itemMain.id,
                contentMobileShift: room.title,
                content: [room.title, room.amount],
                filled: itemMain.filled
              };
            });

            return {
              id: item.id,
              contentMobileShift: item.floor,
              content: item.floor,
              main: main
            };
          })
        }
      });
    case types.TIMELINE_ERROR_FETCHED:
      return state.merge({
        timeline: {}
      });
    case types.SET_CURRENT_DATE:
      return state.merge({
        date: action.date
      });
    case types.SET_TOOLTIP_NODE:
    case types.REMOVE_TOOLTIP_NODE:
      return state.merge({
        toolTipNode: action.node
      });
    default:
      return state;
  }
}
