/**
 * get selected date
 * @param {object} state - global state
 * @return {object} - return date parameter
 * */
function getSelectedDate(state) {
  return state.main.date;
}

/**
 * get timeline date
 * @param {object} state - global state
 * @return {object} - return date parameter
 * */
function getTimeline(state) {
  return state.main.timeline;
}

/**
 * get a tooltip node
 * @param {object} state - global state
 * @return {boolean} - return tooltip node flag
 * */
function getToolTipNode(state) {
  return state.main.toolTipNode;
}

export default {
  getSelectedDate: getSelectedDate,
  getTimeline: getTimeline,
  getToolTipNode: getToolTipNode
};
