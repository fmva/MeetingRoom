import { dispatchRooms } from '../actions';
import configureMockStore from 'redux-mock-store';

const errorAnswer = { error: { status: true, text: 'Error' } };
const dataAnswer = { data: [], error: { status: false, text: '' } };

const mockStore = configureMockStore();

describe('Rooms Actions', () => {
  it('dispatchRooms error', async () => {
    const store = mockStore({});
    dispatchRooms(store.dispatch, errorAnswer);
    expect(store.getActions()[0].type).toBe('ROOMS_ERROR_FETCHED');
    expect(store.getActions()[0].json.error.status).toBeTruthy();
    expect(store.getActions()[0].json.error.text).toBe('Error');
  });

  it('dispatchRooms success', async () => {
    const store = mockStore({});
    dispatchRooms(store.dispatch, dataAnswer);
    expect(store.getActions()[0].type).toBe('ROOMS_SUCCESS_FETCHED');
    expect(store.getActions()[0].json.data.length).toBe(0);
    expect(store.getActions()[0].json.error.status).toBeFalsy();
    expect(store.getActions()[0].json.error.text).toBe('');
  });
});
