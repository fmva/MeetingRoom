import reduce from '../reducer';

const successData = {
  type: 'ROOMS_SUCCESS_FETCHED',
  json: {
    data: [
      {
        id: 1,
        title: 'Washing room',
        amount: 'up to 10 employees',
        floor: 7
      },
      {
        id: 2,
        title: 'Yellow Boat',
        amount: '3 - 6 employees',
        floor: 7
      },
      {
        id: 3,
        title: 'Main room',
        amount: 'up to 20 employees',
        floor: 7
      }
    ]
  }
};
const errorData = {
  type: 'ROOMS_ERROR_FETCHED',
  json: { error: { status: true } }
};

describe('Main reducer', () => {
  it('ROOMS_SUCCESS_FETCHED action', () => {
    const result = reduce(undefined, successData);
    expect(result.data.length).toBe(3);
    expect(result.data[0].id).toBe(1);
    expect(result.data[0].title).toBe('Washing room');
    expect(result.data[0].amount).toBe('up to 10 employees');
    expect(result.data[0].floor).toBe(7);
  });

  it('ROOMS_ERROR_FETCHED action', () => {
    const result = reduce(undefined, errorData);
    expect(result.data).toEqual([]);
  });
});
