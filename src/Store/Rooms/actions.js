import * as types from './action.types';

/**
 * dispatch rooms data
 * @param {function} dispatch - dispatch function
 * @param {object} jsonRooms - rooms json
 * @return {undefined}
 */
export function dispatchRooms(dispatch, jsonRooms) {
  if (jsonRooms.error.status) {
    dispatch({ type: types.ROOMS_ERROR_FETCHED, json: jsonRooms });
  } else {
    dispatch({ type: types.ROOMS_SUCCESS_FETCHED, json: jsonRooms });
  }
}
