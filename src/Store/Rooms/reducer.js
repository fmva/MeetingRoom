import Immutable from 'seamless-immutable';
import * as types from './action.types';

const initialState = Immutable({
  data: []
});

/**
 * reducer of Rooms
 * @param {object} state - state from action
 * @param {object} action - action
 * @return {object} - new state
 */
export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.ROOMS_SUCCESS_FETCHED:
      return state.merge({ data: action.json.data.map(item => item) });
    case types.ROOMS_ERROR_FETCHED:
      return state.merge({
        data: []
      });
    default:
      return state;
  }
}
