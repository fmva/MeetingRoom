/**
 * get rooms
 * @param {object} state - global state
 * @return {object} - return rooms
 * */
function getRooms(state) {
  return state.rooms.data;
}

export default {
  getRooms: getRooms
};
