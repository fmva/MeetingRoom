import { showWait, hideWait } from '../actions';

describe('Wait actions', () => {
  it('showWait action', async () => {
    const result = showWait();

    expect(result.type).toBe('SHOW_WAIT');
    expect(result.isWait).toBeTruthy();
  });

  it('hideWait action', async () => {
    const result = hideWait();

    expect(result.type).toBe('HIDE_WAIT');
    expect(result.isWait).toBeFalsy();
  });
});
