import reduce from '../reducer';

describe('Wait reducer', () => {
  it('default Wait state', () => {
    const result = reduce();

    expect(result.isWait).toBeFalsy();
  });

  it('SHOW_WAIT action', () => {
    const result = reduce(undefined, {
      type: 'SHOW_WAIT',
      isWait: true
    });

    expect(result.isWait).toBeTruthy();
  });

  it('HIDE_WAIT action', () => {
    const result = reduce(undefined, {
      type: 'HIDE_WAIT',
      isWait: false
    });

    expect(result.isWait).toBeFalsy();
  });
});
