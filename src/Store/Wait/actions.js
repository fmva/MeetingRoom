import * as types from './action.types';

/**
 * show wait component
 * @return {object} - return action object
 */
export function showWait() {
  return { type: types.SHOW_WAIT, isWait: true };
}

/**
 * hide wait component
 * @return {object} - return action object
 */
export function hideWait() {
  return { type: types.HIDE_WAIT, isWait: false };
}
