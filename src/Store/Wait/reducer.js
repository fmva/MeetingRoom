import Immutable from 'seamless-immutable';
import * as types from './action.types';

const initialState = Immutable({
  isWait: false
});

/**
 * reducer of Wait container
 * @param {object} state - state from action
 * @param {object} action - action
 * @return {object} - new state
 */
export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.SHOW_WAIT:
    case types.HIDE_WAIT:
      return state.merge({
        isWait: action.isWait
      });

    default:
      return state;
  }
}
