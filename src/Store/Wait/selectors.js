/**
 * get wait parameter from state
 * @param {object} state - global state
 * @return {boolean} - return wait parameter
 * */
function _getWait(state) {
  return state.isWait;
}

export default {
  getWait: _getWait
};
