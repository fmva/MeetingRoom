import main from './Main/reducer';
import wait from './Wait/reducer';
import alertBox from './AlertBox/reducer';
import details from './Details/reducer';
import rooms from './Rooms/reducer';

export { main, wait, alertBox, details, rooms };
