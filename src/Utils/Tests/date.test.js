import {
  getFormattedDate,
  addOneDay,
  subtractOneDay,
  invertStringDate,
  getDateFromString
} from '../date';

describe('Date', () => {
  it('getFormattedDate tests', () => {
    const date = new Date('2019-06-12');

    expect(getFormattedDate(date, 'YYYY-MM-DD')).toBe('2019-06-12');
    expect(getFormattedDate(date, 'YYYY/MM/DD')).toBe('2019/06/12');
    expect(getFormattedDate(date, 'YY/MM/DD')).toBe('19/06/12');
    expect(getFormattedDate(date, 'DD/YY/MM')).toBe('12/19/06');
    expect(getFormattedDate(date, 'DD/MM-YYYY')).toBe('12/06-2019');

    expect(() => {
      getFormattedDate(date, 'DD/MM-');
    }).toThrow();
    expect(() => {
      getFormattedDate(date, 'DD-YYY-MM');
    }).toThrow();
    expect(() => {
      getFormattedDate(date, 'D-YYY-MM');
    }).toThrow();
    expect(() => {
      getFormattedDate(date, 'DD-YYY-M');
    }).toThrow();
    expect(() => {
      getFormattedDate(date, 'DD-YYYYY-M');
    }).toThrow();
    expect(() => {
      getFormattedDate(date, 'DDD-YY-M');
    }).toThrow();
    expect(() => {
      getFormattedDate(date, 'DD-YY-MMM');
    }).toThrow();
  });

  it('getFormattedDate tests', () => {
    const date = addOneDay(new Date('2019-12-17'));

    expect(date.getDate()).toBe(18);
    expect(date.getMonth() + 1).toBe(12);
    expect(date.getFullYear()).toBe(2019);
  });

  it('subtractOneDay tests', () => {
    const date = subtractOneDay(new Date('2019-12-17'));

    expect(date.getDate()).toBe(16);
    expect(date.getMonth() + 1).toBe(12);
    expect(date.getFullYear()).toBe(2019);
  });

  it('invertStringDate tests', () => {
    expect(invertStringDate('2019-12-17')).toBe('17-12-2019');
    expect(invertStringDate('2019-02-17')).toBe('17-02-2019');
    expect(invertStringDate('2019-12-07')).toBe('07-12-2019');
  });

  it('getDateFromString tests', () => {
    const date = getDateFromString('2019-12-17');
    expect(date.getFullYear()).toBe(2019);
    expect(date.getMonth()).toBe(11);
    expect(date.getDate()).toBe(17);

    expect(() => {
      getDateFromString('2019-12-67');
    }).toThrow();
    expect(() => {
      getDateFromString('20195-12-17');
    }).toThrow();
    expect(() => {
      getDateFromString('2019-2-17');
    }).toThrow();
    expect(() => {
      getDateFromString('2019-02-1');
    }).toThrow();
    expect(() => {
      getDateFromString('2019-02');
    }).toThrow();
  });
});
