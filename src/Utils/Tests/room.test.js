import { getRoomById } from '../rooms';

const rooms = [
  {
    id: 1,
    title: 'Washing room',
    amount: 'up to 10 employees',
    floor: 7
  },
  {
    id: 2,
    title: 'Yellow Boat',
    amount: '3 - 6 employees',
    floor: 7
  },
  {
    id: 3,
    title: 'Main room',
    amount: 'up to 20 employees',
    floor: 7
  }
];

describe('Rooms', () => {
  it('getRoomById tests', () => {
    const result = getRoomById(rooms, 2);

    expect(result.id).toBe(2);
    expect(result.title).toBe('Yellow Boat');
    expect(result.amount).toBe('3 - 6 employees');
    expect(result.floor).toBe(7);
  });
});
