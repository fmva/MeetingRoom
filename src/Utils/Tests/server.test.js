import Server from '../server';
import fetch from 'jest-fetch-mock';

global.fetch = fetch;
const url = 'https://localhost';
const json = { results: [] };

describe('Server module', () => {
  afterEach(() => {
    fetch.resetMocks();
  });

  it('if response return status 201', async () => {
    fetch.mockResponseOnce(JSON.stringify(json), { status: 201 });

    const result = await Server.fetch(url);
    expect(result.error.status).toBeTruthy();
    expect(result.error.text).not.toBe('');
    expect(result.data).toBeNull();
  });

  it('if response return status 200', async () => {
    fetch.mockResponseOnce(JSON.stringify(json), { status: 200 });

    const result = await Server.fetch(url);
    expect(result.error.status).toBeFalsy();
    expect(result.error.text).toBe('');
    expect(result.data.results.length).toBe(0);
  });
});
