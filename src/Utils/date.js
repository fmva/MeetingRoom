const THROW_FORMAT_ERROR = 'Wrong format';

const THROW_FORMAT_DATE = {
  wrong_length_numbers: 'Number of the digits is not equal 3!',
  wrong_length_date: 'Number of the digits of the year is not equal 4!',
  wrong_length_days: 'Number of the digits of days or months is not equal 2!',
  wrong_date_format: 'The Date format is wrong!'
};

/**
 * format date to user format
 * @param {Date} date - date
 * @param {string} format - format (e.g. 'YYYY-MM-DD', 'YYYY/MM/DD' and others)
 * @return {string} - formatted date
 *
 */
export function getFormattedDate(date, format = 'YYYY-MM-DD') {
  let month = date.getMonth() + 1,
    day = date.getDate(),
    year = date.getFullYear(),
    yearFormat;

  if (!format.match(/YY/g) || !format.match(/MM/g) || !format.match(/DD/g)) {
    throw THROW_FORMAT_ERROR;
  }
  if (
    format.match(/YY/g).length > 2 ||
    format.match(/MM/g).length > 1 ||
    format.match(/DD/g).length > 1
  ) {
    throw THROW_FORMAT_ERROR;
  }
  if (
    (format.match(/YY/g).length === 1 && format.match(/Y/g).length > 2) ||
    (format.match(/MM/g).length === 1 && format.match(/M/g).length > 2) ||
    (format.match(/DD/g).length === 1 && format.match(/DD/g).length > 2)
  ) {
    throw THROW_FORMAT_ERROR;
  }

  yearFormat = format.match(/YY/g).length === 1 ? 'YY' : 'YYYY';
  year = format.match(/YY/g).length === 1 ? year.toString().substr(2, 2) : year.toString();
  day = day < 10 ? '0' + day : day.toString();
  month = month < 10 ? '0' + month : month.toString();

  return format
    .replace(yearFormat, year)
    .replace('MM', month)
    .replace('DD', day);
}

/**
 * get date from string like 'YYYY-MM-DD'
 * @param {string} date - date like 'YYYY-MM-DD'
 * @return {Date} - formatted date
 */
export function getDateFromString(date) {
  const arrDate = date.split('-');

  if (arrDate.length !== 3) {
    throw THROW_FORMAT_DATE.wrong_length_numbers;
  }
  if (arrDate[0].length !== 4) {
    throw THROW_FORMAT_DATE.wrong_length_date;
  }
  if (arrDate[1].length !== 2 || arrDate[2].length !== 2) {
    throw THROW_FORMAT_DATE.wrong_length_days;
  }

  const newDate = new Date(parseInt(arrDate[0]), parseInt(arrDate[1]) - 1, parseInt(arrDate[2]));
  if (
    newDate.getFullYear() !== parseInt(arrDate[0]) ||
    newDate.getMonth() !== parseInt(arrDate[1]) - 1 ||
    newDate.getDate() !== parseInt(arrDate[2])
  ) {
    throw THROW_FORMAT_DATE.wrong_date_format;
  }

  return newDate;
}

/**
 * increase date on one day
 * @param {Date} date - date
 * @return {object} - increased date
 *
 */
export function addOneDay(date) {
  let localDate = new Date(date.getTime());
  localDate.setDate(localDate.getDate() + 1);
  return localDate;
}

/**
 * subtract one day from date
 * @param {Date} date - date
 * @return {object} - increased date
 *
 */
export function subtractOneDay(date) {
  let localDate = new Date(date.getTime());
  localDate.setDate(localDate.getDate() - 1);
  return localDate;
}

/**
 * invert date like this '2019-02-13' to '13-02-2019'
 * @param {string} stringDate - date
 * @return {string} - inverted date
 *
 */
export function invertStringDate(stringDate) {
  return stringDate.replace(/^(\d{4})-(\d{2})-(\d{2})$/, '$3-$2-$1');
}
