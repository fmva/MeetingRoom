/**
 * get room object by id
 * @param {array} rooms - array object of rooms
 * @param {number} id - id of the room
 * @return {object} - object of the room
 */
export function getRoomById(rooms, id) {
  return rooms.find(itemRoom => id === itemRoom.id);
}
