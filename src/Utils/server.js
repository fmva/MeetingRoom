import 'whatwg-fetch';

/**
 * server format
 * @return {Object} - json format
 *
 */
const result = () => ({
  data: null,
  error: {
    status: false,
    text: ''
  }
});

/**
 * get data from server
 * @param {string} url - url address
 * @return {Object} - json from server
 *
 */
const _fetch = async function(url) {
  let data,
    json = Object.assign(result(), {});

  try {
    data = await fetch(url);
    if (data.status !== 200) {
      json.error.status = true;
      json.error.text = 'Status: ' + data.status;
      return json;
    }
    json.data = await data.json();
  } catch (e) {
    json.error.status = true;
    json.error.text = e.toString();
  }
  return json;
};

/**
 * fetch multiple urls
 * @param {array} urlArr - array of urls
 * @return {Promise<void>} - promise object
 * @private
 */
const _fetchAll = function(urlArr) {
  const requests = urlArr.map(item => {
    return _fetch(item).then(response => {
      return response;
    });
  });
  return new Promise(resolve => {
    Promise.all(requests).then(values => {
      resolve(values);
    });
  });
};

/**
 * fetch multiple urls using async/await
 * @param {array} urlArr - array of urls
 * @return {object} - result json object
 */
const fetchAll = async function(urlArr) {
  return await _fetchAll(urlArr);
};

export default {
  fetch: _fetch,
  fetchAll: fetchAll
};
