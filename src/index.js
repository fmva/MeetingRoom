import '@babel/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { getSettings } from './Config/settings';
import EditForm from './Paths/EditForm/editform';
import Main from './Paths/Main/main';

import './index.scss';

import * as reducers from './Store/reducers';
const store = createStore(combineReducers(reducers), applyMiddleware(thunk));
store.subscribe(() => console.log('store', store.getState()));

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path={`/${getSettings('mainPath')}`} component={Main} />
        <Route path={`/${getSettings('editFormPath')}`} component={EditForm} />
        <Route exact={true} path="/" component={Main} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
